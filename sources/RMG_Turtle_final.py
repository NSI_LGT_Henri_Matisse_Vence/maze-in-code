'''
Projet : Maze in code (Clément K; Tom P; Nolhan G; Camille FD)
Random maze generator
fonctions : erased() , entrance() and maze_function()
modules utilisés : turtle (as t) and random (as r)
Dernière modification : 17/03/2023
'''

import turtle as t
import random as r

t.width(1)
t.shape("classic")
t.speed("fastest")
t.fillcolor('Blue')
maze = {}
nbr_case = 0
n = 500
spawn = (-n / 2, n / 2)

'''fonction erased:
efface un espace compris entre le point A(x1, y1) et le point B(x2, y2)
'''

def erased(width, x1, y1, x2, y2):
    if x1 == x2:
        y1 -= 1
    else:
        x1 += 1
    t.width(width)
    t.up()
    t.goto(x1, y1)
    t.pencolor('white')
    t.down()
    t.goto(x2, y2)
    t.up()
    t.pencolor('black')
    t.width(1)

'''Construit la base du labyrinthe : le carré, l'entrée et la sortie :'''

def entrance(p):
    global spawn
    t.up()
    t.goto(spawn)
    t.width(3)
    t.down()
    t.pencolor('black')

    '''construit le carré'''

    for i in range(4):
        t.forward(n)
        t.right(90)

    '''ouvre l'entrée et la sortie'''

    '''pour l'entrée'''
    erased(3, spawn[0], spawn[1], -n / 2, n / 2 - n / p)

    '''pour la sortie'''
    erased(3, -spawn[0], -spawn[1], n / 2, -n / 2 + n / p)


'''maze_function:
construit un labyrinthe en utilisant comme parametre p correspondant au nombre de cases d'un coté du labyrinthe
'''

def maze_function(p):
    global n
    nbr_case = p ** 2
    j = 0


    '''wn permet de gerer la mise à jour de l'ecran, de se fait on gagne du temps sur l'affichage'''
    wn = t.Screen()
    wn.tracer(0)


    '''construction du carré encadrant, de l'entrée et de la sortie'''
    entrance(p)


    wn.update()


    '''construction de la grille :'''

    '''colonnes'''
    for i in range(1, p):
        t.up()
        t.goto(-n / 2 + i * (n / p), n / 2)
        t.down()
        t.goto(-n / 2 + i * (n / p), -n / 2)

    '''lignes'''
    for i in range(1, p):
        t.up()
        t.goto(-n / 2, n / 2 - i * (n / p))
        t.down()
        t.goto(n / 2, n / 2 - i * (n / p))
    wn.update()

    '''initialise le "maze" : le dictionnaire contenant toutes les cases'''

    '''pour les lignes'''
    for k in range(p):

        '''pour les colonnes'''
        for i in range(p):

            '''
            case n° = numero de la case
            pos1x = coordonnée x du coin superieur gauche de la case
            pos1y = coordonnée y du coin superieur gauche de la case
            pos2x = coordonnée x du coin inferieur droit de la case
            pos2y = coordonnée y du coin inferieur droit de la case
            x = coordonnée x du centre de la case
            y = coordonnée y du centre de la case
            /after
            '''
            maze[j] = {'case n°': j + 1,
                       'pos1x': (-n / 2 + i * n / p),
                       'pos1y': (n / 2 - k * n / p),
                       'pos2x': (-n / 2 + (i + 1) * n / p),
                       'pos2y': (n / 2 - (k + 1) * n / p),
                       'x': ((-n / 2 + i * n / p) + (-n / 2 + (i + 1) * n / p)) / 2,
                       'y': ((n / 2 - k * n / p) + (n / 2 - (k + 1) * n / p)) / 2,
                       'own_nbr': j,
                       }
            j += 1

    rdm_case = 0 # numero de la case sur laquelle est situé le curseur

    rdm_nbr = r.randint(1, 4) # nombre aléatoire compris entre 1 et 4 (1 et 4 compris)

    all = 1 # nombre de cases reliées entre elles


    """boucle while pour créer les chemins du labyrinthe en cassant des murs, 
    elle ne s'arrete qu'à l'instant où tous les murs du labyrinthe sont connectés ensemble"""

    while all < nbr_case:

        rdm_own = maze[rdm_case]['own_nbr']
        count = 0


        '''lorsque rdm_nbr vaut 1, le curseur va chercher à casser le mur situé à gauche de la case actuelle'''
        if rdm_nbr == 1:
            if maze.get(rdm_case -1) is not None and maze[rdm_case - 1]['own_nbr'] != maze[rdm_case]['own_nbr'] and maze[rdm_case]['pos1x'] == maze[rdm_case - 1]['pos2x']:
                erased(1, maze[rdm_case]['pos1x'], maze[rdm_case]['pos1y'], maze[rdm_case]['pos1x'],
                       maze[rdm_case]['pos2y'])
                maze[rdm_case - 1]['own_nbr'] = maze[rdm_case]['own_nbr']
                rdm_case = rdm_case - 1
                all += 1
            else:
                rdm_nbr = r.randint(1, 4)
                count += 1


        '''lorsque rdm_nbr vaut 2, le curseur va chercher à casser le mur situé au dessus de la case actuelle'''
        if rdm_nbr == 2:
            if maze.get(rdm_case - p) is not None and maze[rdm_case - p]['own_nbr'] != maze[rdm_case]['own_nbr']:
                erased(1, maze[rdm_case]['pos1x'], maze[rdm_case]['pos1y'], maze[rdm_case - p]['pos2x'],
                       maze[rdm_case - p]['pos2y'])
                maze[rdm_case - p]['own_nbr'] = maze[rdm_case]['own_nbr']
                rdm_case = rdm_case - p
                all += 1
            else:
                rdm_nbr = r.randint(1, 4)
                count += 1


        '''lorsque rdm_nbr vaut 3, le curseur va chercher à casser le mur situé à droite de la case actuelle'''
        if rdm_nbr == 3:
            if maze.get(rdm_case + 1) is not None and maze[rdm_case + 1]['own_nbr'] != maze[rdm_case]['own_nbr'] and \
                    maze[rdm_case]['pos2x'] == maze[rdm_case + 1]['pos1x']:
                erased(1, maze[rdm_case + 1]['pos1x'], maze[rdm_case + 1]['pos1y'], maze[rdm_case]['pos2x'],
                       maze[rdm_case]['pos2y'])
                maze[rdm_case + 1]['own_nbr'] = maze[rdm_case]['own_nbr']
                rdm_case = rdm_case + 1
                all += 1
            else:
                rdm_nbr = r.randint(1, 4)
                count += 1


        '''lorsque rdm_nbr vaut 4, le curseur va chercher à casser le mur situé en dessous de la case actuelle'''
        if rdm_nbr == 4:
            if maze.get(rdm_case + p) is not None and maze[rdm_case + p]['own_nbr'] != maze[rdm_case]['own_nbr']:
                erased(1, maze[rdm_case + p]['pos1x'], maze[rdm_case + p]['pos1y'], maze[rdm_case]['pos2x'],
                       maze[rdm_case]['pos2y'])
                maze[rdm_case + p]['own_nbr'] = maze[rdm_case]['own_nbr']
                rdm_case = rdm_case + p
                all += 1


            #si count vaut 3 alors aucun mur environnant de peux etre cassé, le curseur va donc se déplacer vers une autre case
            elif count == 3:
                al = r.randint(0, len(maze) - 1)
                while maze[al]['own_nbr'] != maze[rdm_case]['own_nbr']:
                    al = r.randint(0, len(maze) - 1)
                rdm_case = al
                maze[rdm_case]['own_nbr'] = rdm_own
                wn.update()

            else:
                rdm_nbr = r.randint(1, 3)
                continue


        rdm_nbr = r.randint(1, 4)

        '''affiche l'avancement de la génération'''
        print(all/nbr_case)

    entrance(p)

    wn.update()

    t.mainloop()


'''appel de la fonction'''
maze_function(50)