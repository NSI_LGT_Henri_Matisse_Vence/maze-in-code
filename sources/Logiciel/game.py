import pygame
from random import *
from player import Player
from monster import Monster
from map_RPG import Map
from sounds import SoundManager
from RMR_jeu import *

from time import *


# créer la classe représentant le jeu
class Game:

    def __init__(self):

        # gerer le son
        self.sound_manager = SoundManager()

        #def si le jeu Escape a commencé
        self.Escape_is_playing = False
        
        #def si le jeu RPG a commencé
        self.RPG_is_playing = False

        self.RPG_cin1_is_playing = False
        self.RPG_cin2_is_playing = False

        self.RPG_floor_1a_initialisation = False
        self.RPG_floor_1a_is_playing = False

        self.RPG_floor_1b_initialisation = False
        self.RPG_floor_1b_is_playing = False

        # def si le RMG a commencé
        self.RMG_is_playing = False
        self.RMG_maze_map = None
        self.RMG_maze = None
        self.mazesolved_done = False

        self.music = None
        self.time_music = 0

        #police d'ecriture
        self.Escape_counter_police = pygame.font.Font("assets/police/Gras.ttf", 20)

        self.RPG_texte_police = pygame.font.Font("assets/police/Regular.ttf", 15)
        self.RPG_health_police = pygame.font.Font("assets/police/Regular.ttf", 10)
        self.RPG_stats_police = pygame.font.Font("assets/police/Regular.ttf", 12)
        self.RPG_pseudo_monster_basic_police = pygame.font.Font("assets/police/Regular.ttf", 12)
        self.RPG_level_police = pygame.font.Font("assets/police/Regular.ttf", 12)


        # generer le joueur
        self.all_players = pygame.sprite.Group()
        self.player = Player(self)
        self.all_players.add(self.player)

        # creer un groupe de monstre
        self.all_monsters = pygame.sprite.Group()


        # generer la map du RPG
        self.map_RPG = Map(self)

        #initialiser le dict qui contiendra les touches actionnées
        self.pressed = {}

        #variable comptant le nombre d'occurence de la boucle de jeu
        self.occ = 0


    '''methode pour le jeu Escape'''
    def updateEscape(self, screen, ti, im_maze):
        self.occ += 1
    

        # appliquer le labyrinthe du jeu
        screen.blit(im_maze, (screen.get_size()[0] / 2 - im_maze.get_size()[0] / 2, screen.get_size()[1] / 2 - im_maze.get_size()[1] / 2))


        #changer la couleur du timer en fonction du temps
        if 255 - self.occ * 0.015 >= 0:
            colcounter = (0 + self.occ * 0.015, 0, 255 - self.occ * 0.015)
        else:
            colcounter = (255, 0, 0)


        # appliquer le timer
        myfont = self.Escape_counter_police
        text = str(round((time() - ti) // 60)) + " min " + str(round((time() - ti) % 60, 1)) + "s   " + "speed :" + str(self.player.velocity)

        score_display = myfont.render(text, 1, colcounter)

        screen.blit(score_display, (screen.get_size()[0] // 2 - score_display.get_size()[0] // 2, 25))


        # joueur

        # appliquer image du joueur
        self.player.image = pygame.transform.scale(self.player.image, (self.player.size[0], self.player.size[1]))
        screen.blit(self.player.image, self.player.rect)


        # verifier les mouvements du joueur 1 fois sur 5 pour réguler la vitesse
        if self.occ % 5 == 0:

            # variable ver verifiant si le joueur rencontre un mur
            ver = True

            #aller à droite
            if self.pressed.get(pygame.K_RIGHT):
                for x in range(self.player.get_velocity()):
                    for y in range(self.player.rect[3]):
                        if screen.get_at((self.player.rect.x + self.player.rect[2] + 1 - x,
                                          self.player.rect.y + self.player.rect[3] - y)) == (0, 0, 0):
                            ver = False
                if ver:
                    self.player.move_right()

            ver = True

            #aller à gauche
            if self.pressed.get(pygame.K_LEFT):
                for x in range(self.player.get_velocity()):
                    for y in range(self.player.rect[3]):
                        if screen.get_at(
                                (self.player.rect.x - 2 - x, self.player.rect.y + self.player.rect[3] - y)) == (
                        0, 0, 0):
                            ver = False

                if ver and self.player.rect.x > 230:
                    self.player.move_left()

            ver = True

            #aller en bas
            if self.pressed.get(pygame.K_DOWN):
                for x in range(self.player.rect[2]):
                    for y in range(self.player.get_velocity()):
                        if screen.get_at(
                                (self.player.rect.x + x - 1, self.player.rect.y + self.player.rect[3] + 1 + y)) == (
                        0, 0, 0):
                            ver = False
                if ver:
                    self.player.move_down()

            ver = True

            # aller en haut
            if self.pressed.get(pygame.K_UP):
                for x in range(self.player.rect[2]):
                    for y in range(self.player.get_velocity()):
                        if screen.get_at((self.player.rect.x + x - 1, self.player.rect.y - 2 - y)) == (0, 0, 0):
                            ver = False
                if ver:
                    self.player.move_up()

            # changer la vitesse avec la touche SPACE

            if self.pressed.get(pygame.K_x):
                self.player.change_velocity(3)
                self.player.intRun = 1

            else:
                self.player.change_velocity(2)
                self.player.intRun = 1


    '''methode pour le jeu RPG'''
    def updateRPG(self, screen, ti, maze, dicmaze):


        ''' gerer le déoulement dans le lieu d'apparition du joueur'''
        if self.RPG_cin1_is_playing:

            self.music = 'explosion_floor_1'
            if self.occ == 0:
                self.sound_manager.volume(self.music, 0.2)

            self.occ += 1
            ver_move = False

            if self.occ % 150 == 0:
                self.sound_manager.restart(self.music)


            screen.blit(self.map_RPG.image, (self.map_RPG.rect.x, self.map_RPG.rect.y))

            self.message(screen,
                         "Dirige toi grâce aux flèches directionnelles",
                         (screen.get_size()[0] / 2, 50))

            if self.pressed.get(pygame.K_RIGHT):
                self.map_RPG.move_right()
                ver_move = True


            if self.map_RPG.rect.x < 0 and self.pressed.get(pygame.K_LEFT):
                self.map_RPG.move_left()
                ver_move = True

            self.player.resize(2)
            self.player.goto(screen.get_size()[0] / 2 - self.player.size[0],
                             screen.get_size()[1] / 2 - self.player.size[1])
            screen.blit(self.player.image, self.player.rect)

            # changer la velocity du joueur avec la touche x
            if self.pressed.get(pygame.K_x):
                self.map_RPG.velocity = self.map_RPG.max_velocity
            else:
                self.map_RPG.velocity = self.map_RPG.min_velocity

            if self.map_RPG.rect.x <= - self.map_RPG.size[0]/2.1 and self.map_RPG.rect.x >= - self.map_RPG.size[0]/1.9:

                self.map_RPG.velocity = 1
                self.message(screen, "Bienvenue.. / dans le Labyrinthe..", (screen.get_size()[0] / 2, screen.get_size()[1] / 1.5))

                if not ver_move:
                    self.player.image = self.player.dicim['up']

            else:
                self.map_RPG.velocity = 2

            if self.map_RPG.rect.x <= - self.map_RPG.size[0]:
                self.RPG_cin1_is_playing = False



            if not self.RPG_cin1_is_playing:

                self.map_RPG.change_map(pygame.image.load(maze[0]))
                self.map_RPG.save_rect = self.map_RPG.image.get_rect()
                self.map_RPG.center(screen, maze)

                # repositionnement et appliquer l'image du joueur au début du labyrinthe
                self.player.goto(screen.get_size()[0] / 2 - self.player.size[0],
                                 screen.get_size()[1] / 2 - self.player.size[1])
                self.map_RPG.goto(self.player.rect.x, self.map_RPG.rect.y)

                self.RPG_floor_1a_initialisation = True


        '''initialiser le 1er niveau du 1er étage'''
        if self.RPG_floor_1a_initialisation:

            # initialisation des monstres

            for ghost in range(1):
                self.spawn_monster(name='Ghost_black', health=40, attack=15, defense=15, velocity=2, xp_dropped=5)

            for bat in range(6):
                self.spawn_monster(name='Bat', health=40, attack=10, defense=10, velocity=2, xp_dropped=5)

            for gob in range(6):
                self.spawn_monster(name='Green_gobelin', health=70, attack=15, defense=10, velocity=2, xp_dropped=5)

            for slime in range(3):
                self.spawn_monster(name='Slime_pink', health=20, attack=10, defense=5, velocity=2, xp_dropped=2)

            for monster in self.all_monsters:
                random_case = randint(1, len(dicmaze) - 1)
                print(dicmaze[random_case])
                monster.goto(dicmaze[random_case]['x'] + self.map_RPG.rect.x,
                             dicmaze[random_case]['y'] + self.map_RPG.rect.y)

            # music
            self.music = 'floor_1'
            self.sound_manager.volume('floor_1', volume_value= 0.02)
            self.sound_manager.play('floor_1')
            self.time_music = time()

            self.sound_manager.volume('slash', volume_value= 0.01)

            self.sound_manager.volume('death_monster', volume_value= 0.01)

            self.sound_manager.volume('teleportation', volume_value= 0.05)

            self.RPG_floor_1a_initialisation = False
            self.RPG_cin2_is_playing = True


        ''' faire avancer le joueur au centre de la 1ere case'''
        if self.RPG_cin2_is_playing:

            self.occ += 1
            dicmaze = maze[1]

            screen.blit(self.map_RPG.image, (self.map_RPG.rect.x, self.map_RPG.rect.y))
            self.player.resize(2)
            self.player.goto(screen.get_size()[0] / 2 - self.player.size[0], screen.get_size()[1] / 2 - self.player.size[1])
            screen.blit(self.player.image, self.player.rect)

            if self.player.rect.x < dicmaze[0]['x'] + self.map_RPG.rect.x:
                if self.occ % 10 == 0:
                    for i in range(3):
                        self.map_RPG.move_right()


            self.message(screen, "Maintiens 'x' pour courir et 'e' pour attaquer / (presse la touche 'espace' pour continuer)",
                         (screen.get_size()[0] / 2, screen.get_size()[1] / 1.2))


            if self.pressed.get(pygame.K_SPACE) and self.player.rect.x >= dicmaze[0]['x'] + self.map_RPG.rect.x:
                self.RPG_cin2_is_playing = False

            if self.RPG_cin2_is_playing == False:
                self.RPG_floor_1a_is_playing = True


        '''gerer le déroulement du 1er niveau du 1er étage'''
        if self.RPG_floor_1a_is_playing:

            self.occ += 1
            dicmaze = maze[1]

            # music
            if time() - self.time_music > 86:
                self.sound_manager.restart(self.music)
                self.time_music = time()


            # appliquer image du labyrinthe
            screen.blit(self.map_RPG.image, (self.map_RPG.rect.x, self.map_RPG.rect.y))

            # redimensionnement du joueur
            self.player.resize(2)

            # repositionnement et appliquer l'image du joueur au milieu
            self.player.goto(screen.get_size()[0] / 2 - self.player.size[0],
                             screen.get_size()[1] / 2 - self.player.size[1])
            screen.blit(self.player.image, self.player.rect)

            # mouvements du joueur
            self.update_move_player(screen= screen)


            # regenerer progressivement la vie
            if self.player.health < self.player.max_health:
                self.player.health += self.player.health_regeneration

            # regenerer progressivement la stamina
            if self.player.stamina < self.player.max_stamina:
                self.player.stamina += self.player.stamina_regeneration


            # gerer les attaques du joueur :
            if self.pressed.get(pygame.K_e):

                self.player.unsheathe(surface= screen, weapon_name= 'longsword', weapon_damage= 20, weapon_couldown= 10)

                if self.player.couldown_count > self.player.couldown:

                    for monster in self.all_monsters:

                        if self.distance_entity(self.player, monster) < 50:
                            self.player.attack_entity(monster)

                    self.player.couldown_count = 0

                self.player.couldown_count += 1

            else:
                self.player.weapon_equiped = None

            # appliquer la barre de vie
            self.player.update_health_bar(screen)

            # appliquer la barre d'experience (xp)
            self.player.update_xp_bar(screen)

            # appliquer le niveau
            self.player.update_level_counter(screen)

            # passage au niveau superieur si l'xp est suffisante
            self.player.new_level()




            '''Actions des monstres stockés dans le groupe de sprites "all_monsters"'''

            for monster in self.all_monsters:

                # mort du monstre
                if monster.health <= 0:
                    self.sound_manager.play('death_monster')
                    random_case = randint(0, len(dicmaze) - 1)
                    monster.goto(dicmaze[random_case]['x'] + self.map_RPG.rect.x, dicmaze[random_case]['y'] + self.map_RPG.rect.y)

                    monster.health = monster.max_health
                    monster.level_up()


                '''verifier si le monstre est assez pret'''
                if self.distance_entity(self.player, monster) < 2200:

                    '''gerer les actions du Ghost_black après 100 occurences'''
                    if self.occ > 100 and monster.name == 'Ghost_black':


                        if monster.rect.x < self.player.rect.x:
                            monster.right()

                        if monster.rect.x > self.player.rect.x:
                            monster.left()

                        if monster.rect.y < self.player.rect.y:
                            monster.down()

                        if monster.rect.y > self.player.rect.y:
                            monster.up()

                        monster.verification_animation = True

                        monster.resize(1.5)

                        screen.blit(monster.image, (monster.rect.x, monster.rect.y))


                '''verifier si le monstre est assez pret'''
                if self.distance_entity(self.player, monster) < 1000:

                    '''gerer les actions de ... après 100 occurences'''
                    if monster.name == 'Bat' or monster.name == 'Green_gobelin' or monster.name == 'Troll' or monster.name == 'Vampire' or monster.name == 'Slime_pink':


                        ver = True

                        if monster.rect.x < self.player.rect.x:

                            for x in range(monster.velocity):
                                for y in range(monster.size[1]):
                                    if self.map_RPG.image.get_at((monster.rect.x - self.map_RPG.rect.x + monster.size[0] + x,
                                                                  monster.rect.y - self.map_RPG.rect.y + y)) == (0, 0, 0):
                                        ver = False

                            if ver:
                                monster.right()

                        ver = True

                        if monster.rect.x > self.player.rect.x:

                            for x in range(monster.velocity):
                                for y in range(monster.size[1]):
                                    if self.map_RPG.image.get_at((monster.rect.x - self.map_RPG.rect.x - x - 1,
                                                                  monster.rect.y - self.map_RPG.rect.y + y)) == (0, 0, 0):
                                        ver = False

                            if ver:
                                monster.left()

                        ver = True

                        if monster.rect.y < self.player.rect.y:

                            for x in range(monster.size[0]):
                                for y in range(monster.velocity):
                                    if self.map_RPG.image.get_at((monster.rect.x  - self.map_RPG.rect.x + x,
                                                                  monster.rect.y - self.map_RPG.rect.y + monster.size[1] + y)) == (0, 0, 0):
                                        ver = False

                            if ver:
                                monster.down()

                        ver = True

                        if monster.rect.y > self.player.rect.y:

                            for x in range(monster.size[0]):
                                for y in range(monster.velocity):
                                    if self.map_RPG.image.get_at((monster.rect.x - self.map_RPG.rect.x + x,
                                                                  monster.rect.y - self.map_RPG.rect.y - y)) == (0, 0, 0):
                                        ver = False

                            if ver:
                                monster.up()

                        if monster.name == 'Bat':
                            monster.resize(0.5)

                        elif monster.name == 'Green_gobelin':
                            monster.resize(2)

                        elif monster.name == 'Troll':
                            monster.resize(2.5)

                        elif monster.name == 'Vampire':
                            monster.resize(2)

                        monster.verification_animation = True

                        screen.blit(monster.image, (monster.rect.x, monster.rect.y))

                # affichage de la barre de vie
                monster.update_health_bar(screen)

                # affichage du pseudo
                monster.pseudo(screen)


            # afficher les informations sur le joueur
            self.update_font_stat(screen)



            ''' arrete le jeu si le joueur est mort'''
            if self.player.health <= 0:

                self.sound_manager.play('game_over')

                ti = time() - ti

                self.RPG_floor_1a_is_playing = False
                self.end_game_RPG()

                self.message(screen, "Game Over...",
                             (screen.get_size()[0] / 2, screen.get_size()[1] / 2))
                pygame.display.flip()
                sleep(2)

                '''arrete le jeu si le joueur atteint la sortie'''
            elif self.map_RPG.rect.x < screen.get_size()[0] / 2 - self.map_RPG.size[0] + 3:

                self.RPG_floor_1a_is_playing = False
                self.RPG_floor_1b_initialisation = True

                self.message(screen, "Nouveau niveau !",
                             (screen.get_size()[0] / 2, screen.get_size()[1] / 2))
                pygame.display.flip()
                sleep(2)


        ''' initialiser le 2nd niveau du 1er étage'''
        if self.RPG_floor_1b_initialisation:


            # initialisation des monstres

            self.all_monsters = pygame.sprite.Group()

            for troll in range(6):
                self.spawn_monster(name='Troll', health=300, attack=60, defense=10, velocity=2, xp_dropped= 30)

            for vamp in range(2):
                self.spawn_monster(name='Vampire', health= 400, attack=70, defense=20, velocity=2, xp_dropped= 45)


            self.map_RPG.goto(self.player.rect.x, self.player.rect.y - 50)


            screen.blit(self.map_RPG.image, (self.map_RPG.rect.x, self.map_RPG.rect.y))
            self.player.resize(2)
            self.player.goto(screen.get_size()[0] / 2 - self.player.size[0],
                             screen.get_size()[1] / 2 - self.player.size[1])
            screen.blit(self.player.image, self.player.rect)


            for monster in self.all_monsters:

                random_case = randint(1, len(dicmaze) - 1)
                print(dicmaze[random_case])
                monster.goto(dicmaze[random_case]['x'] + self.map_RPG.rect.x,
                             dicmaze[random_case]['y'] + self.map_RPG.rect.y)


            self.RPG_floor_1b_initialisation = False
            self.RPG_floor_1b_is_playing = True


        ''' gérer le déroulement du 2nd niveau du 1er étage '''
        if self.RPG_floor_1b_is_playing:

            self.occ += 1
            dicmaze = maze[1]

            # music
            if time() - self.time_music > 86:
                self.sound_manager.restart(self.music)
                self.time_music = time()


            # appliquer image du labyrinthe
            screen.blit(self.map_RPG.image, (self.map_RPG.rect.x, self.map_RPG.rect.y))

            # redimensionnement du joueur
            self.player.resize(2)

            # repositionnement et appliquer l'image du joueur au milieu
            self.player.goto(screen.get_size()[0] / 2 - self.player.size[0],
                             screen.get_size()[1] / 2 - self.player.size[1])
            screen.blit(self.player.image, self.player.rect)

            # mouvements du joueur
            self.update_move_player(screen= screen)

            # regenerer progressivement la vie
            if self.player.health < self.player.max_health:
                self.player.health += self.player.health_regeneration

            # regenerer progressivement la stamina
            if self.player.stamina < self.player.max_stamina:
                self.player.stamina += self.player.stamina_regeneration


            # gerer les attaques du joueur :
            if self.pressed.get(pygame.K_e):

                self.player.unsheathe(surface=screen, weapon_name='longsword', weapon_damage=20, weapon_couldown=10)

                if self.player.couldown_count > self.player.couldown:

                    for monster in self.all_monsters:

                        if self.distance_entity(self.player, monster) < 50:
                            self.player.attack_entity(monster)

                    self.player.couldown_count = 0

                self.player.couldown_count += 1

            else:
                self.player.weapon_equiped = None


            # appliquer la barre de vie
            self.player.update_health_bar(screen)

            # appliquer la barre d'experience (xp)
            self.player.update_xp_bar(screen)

            # appliquer le niveau
            self.player.update_level_counter(screen)

            # passage au niveau superieur si l'xp est suffisante
            self.player.new_level()


            '''Actions des monstres stockés dans le groupe de sprites "all_monsters"'''

            for monster in self.all_monsters:

                # mort du monstre
                if monster.health <= 0:
                    self.sound_manager.play('death_monster')
                    random_case = randint(0, len(dicmaze) - 1)
                    monster.goto(dicmaze[random_case]['x'] + self.map_RPG.rect.x,
                                 dicmaze[random_case]['y'] + self.map_RPG.rect.y)

                    monster.health = monster.max_health
                    monster.level_up()

                '''verifier si le monstre est assez pret'''
                if self.distance_entity(self.player, monster) < 2200:

                    '''gerer les actions du Ghost_black après 100 occurences'''
                    if monster.name == 'Ghost_black':

                        if monster.rect.x < self.player.rect.x:
                            monster.right()

                        if monster.rect.x > self.player.rect.x:
                            monster.left()

                        if monster.rect.y < self.player.rect.y:
                            monster.down()

                        if monster.rect.y > self.player.rect.y:
                            monster.up()

                        monster.verification_animation = True

                        monster.resize(1.5)

                        screen.blit(monster.image, (monster.rect.x, monster.rect.y))


                '''verifier si le monstre est assez pret'''
                if self.distance_entity(self.player, monster) < 1000:

                    '''gerer les actions de ... après 100 occurences'''
                    if monster.name == 'Bat' or monster.name == 'Green_gobelin' or monster.name == 'Troll' or monster.name == 'Vampire' or monster.name == 'Slime_pink':


                        ver = True

                        if monster.rect.x < self.player.rect.x:

                            for x in range(monster.velocity):
                                for y in range(monster.size[1]):
                                    if self.map_RPG.image.get_at(
                                            (monster.rect.x - self.map_RPG.rect.x + monster.size[0] + x,
                                             monster.rect.y - self.map_RPG.rect.y + y)) == (0, 0, 0):
                                        ver = False

                            if ver:
                                monster.right()

                        ver = True

                        if monster.rect.x > self.player.rect.x:

                            for x in range(monster.velocity):
                                for y in range(monster.size[1]):
                                    if self.map_RPG.image.get_at((monster.rect.x - self.map_RPG.rect.x - x - 1,
                                                                  monster.rect.y - self.map_RPG.rect.y + y)) == (
                                    0, 0, 0):
                                        ver = False

                            if ver:
                                monster.left()

                        ver = True

                        if monster.rect.y < self.player.rect.y:

                            for x in range(monster.size[0]):
                                for y in range(monster.velocity):
                                    if self.map_RPG.image.get_at((monster.rect.x - self.map_RPG.rect.x + x,
                                                                  monster.rect.y - self.map_RPG.rect.y + monster.size[
                                                                      1] + y)) == (0, 0, 0):
                                        ver = False

                            if ver:
                                monster.down()

                        ver = True

                        if monster.rect.y > self.player.rect.y:

                            for x in range(monster.size[0]):
                                for y in range(monster.velocity):
                                    if self.map_RPG.image.get_at((monster.rect.x - self.map_RPG.rect.x + x,
                                                                  monster.rect.y - self.map_RPG.rect.y - y)) == (
                                    0, 0, 0):
                                        ver = False

                            if ver:
                                monster.up()

                        if monster.name == 'Bat':
                            monster.resize(0.5)

                        elif monster.name == 'Green_gobelin':
                            monster.resize(2)

                        elif monster.name == 'Troll':
                            monster.resize(2.5)

                        elif monster.name == 'Vampire':
                            monster.resize(2)

                        monster.verification_animation = True

                        screen.blit(monster.image, (monster.rect.x, monster.rect.y))

                # affichage de la barre de vie
                monster.update_health_bar(screen)

                # affichage du pseudo
                monster.pseudo(screen)


            # afficher les informations sur le joueur
            self.update_font_stat(screen)


            # verifier et arreter le jeu si e joueur est mort ou a atteint la sortie
            if self.player.health <= 0:

                self.sound_manager.play('game_over')

                ti = time() - ti

                self.RPG_floor_1b_is_playing = False
                self.end_game_RPG()
                self.message(screen, "Game Over...",
                             (screen.get_size()[0] / 2, screen.get_size()[1] / 2))
                pygame.display.flip()
                sleep(2)

            elif self.map_RPG.rect.x < screen.get_size()[0] / 2 - self.map_RPG.size[0] + 3:

                self.RPG_floor_1b_is_playing = False
                self.end_game_RPG()
                self.message(screen, "Félicitation !",
                             (screen.get_size()[0] / 2, screen.get_size()[1] / 2))
                pygame.display.flip()
                sleep(2)



    '''methode pour le RMG (Random Maze Generator / Generateur de labyrinthe aléatoire) et le RMR (Random Maze Resolveur / Solveur de labyrinthe aléatoire)'''
    def updateRMG(self, screen, maze):

        screen.blit(self.RMG_maze_map, (screen.get_size()[0]/2 - self.RMG_maze_map.get_size()[0]/2, screen.get_size()[1]/2 - self.RMG_maze_map.get_size()[1]/2))
        self.message(screen, "appuyer sur la touche t pour retourner à l'écran d'accueil", (screen.get_size()[0] / 2, screen.get_size()[1] - 10))

        if self.pressed.get(pygame.K_SPACE) and not self.mazesolved_done:

            self.RMG_maze_map = pygame.image.load(maze_res_f((maze[2], maze[1])))

            self.mazesolved_done = True

        elif not self.mazesolved_done:
            self.message(screen, "appuyer sur la touche espace pour résoudre ce labyinthe", (screen.get_size()[0]/2, 50))


    '''mouvement basique du joueur'''
    def update_move_player(self, screen):


        '''gerer les actions du joueur'''


        # changer la velocity du joueur avec la touche x
        if self.pressed.get(pygame.K_x) and self.player.stamina > self.player.max_stamina * 0.05:
            self.map_RPG.velocity = self.map_RPG.max_velocity
        else:
            self.map_RPG.velocity = self.map_RPG.min_velocity


        # variable ver verifiant si le joueur rencontre un mur (True si aucun mur n'est detecté et False si un mur est détécté)
        ver = True

        # aller à droite
        if self.pressed.get(pygame.K_RIGHT):

            for x in range(3):
                for y in range(self.player.rect[3]):
                    if screen.get_at((self.player.rect.x + self.player.rect[2] + x,
                                      self.player.rect.y + self.player.rect[3] - y)) == (0, 0, 0):
                        ver = False

            if ver:

                self.map_RPG.move_right()

                if self.map_RPG.get_velocity() == self.map_RPG.max_velocity:
                    self.player.stamina -= 0.1

        ver = True

        # aller à gauche
        if self.pressed.get(pygame.K_LEFT):

            for x in range(3):
                for y in range(self.player.rect[3]):
                    if screen.get_at(
                            (self.player.rect.x - 4 - x, self.player.rect.y + self.player.rect[3] - y)) == (
                            0, 0, 0):
                        ver = False

            if ver and self.player.rect.x > 218:

                self.map_RPG.move_left()

                if self.map_RPG.get_velocity() == self.map_RPG.max_velocity:
                    self.player.stamina -= 0.1

        ver = True

        # aller en bas
        if self.pressed.get(pygame.K_DOWN):

            for x in range(self.player.rect[2]):
                for y in range(3):
                    if screen.get_at(
                            (self.player.rect.x + x - 1, self.player.rect.y + self.player.rect[3] + 3 + y)) == (
                            0, 0, 0):
                        ver = False

            if ver:

                self.map_RPG.move_down()

                if self.map_RPG.get_velocity() == self.map_RPG.max_velocity:
                    self.player.stamina -= 0.1

        ver = True

        # aller en haut
        if self.pressed.get(pygame.K_UP):

            for x in range(self.player.rect[2]):
                for y in range(3):
                    if screen.get_at((self.player.rect.x + x - 1, self.player.rect.y - 1 - y)) == (0, 0, 0):
                        ver = False

            if ver:

                self.map_RPG.move_up()

                if self.map_RPG.get_velocity() == self.map_RPG.max_velocity:
                    self.player.stamina -= 0.1


    '''afficher la vie, la stamina, l'attaque et la defense du joueur'''
    def update_font_stat(self, screen):

        '''Affichage permanent des informations principales'''
        health_ratio = 0

        font_stat = pygame.image.load('assets/map/back_black.png')
        font_stat = pygame.transform.scale(font_stat, (200, 250))

        # health bar
        health_bar = pygame.image.load('assets/stats/health_bar.png')
        health_bar = pygame.transform.scale(health_bar,
                                            (health_bar.get_size()[0] * 0.12, health_bar.get_size()[1] * 0.12))

        if self.player.health / self.player.max_health >= 0:
            health_ratio = self.player.health / self.player.max_health

        # stamina bar
        stamina_bar = pygame.image.load('assets/stats/stamina_bar.png')
        stamina_bar = pygame.transform.scale(stamina_bar,
                                             (stamina_bar.get_size()[0] * 0.06, stamina_bar.get_size()[1] * 0.06))

        if self.player.stamina / self.player.max_stamina >= 0:
            stamina_ratio = self.player.stamina / self.player.max_stamina
        else:
            self.player.stamina = 0
            stamina_ratio = 0

        # font de l'affichage en haut à gauche
        pygame.draw.rect(font_stat, "black", (0, 0, font_stat.get_size()[0], font_stat.get_size()[1]))
        pygame.draw.rect(font_stat, "white", (0, 0, font_stat.get_size()[0] * 0.97, font_stat.get_size()[1] * 0.97))

        # affichage de la barre de vie
        pygame.draw.rect(health_bar, "black", (35, 15, 95, 9))
        pygame.draw.rect(health_bar, "red", (35, 15, 95 * health_ratio, 9))
        font_stat.blit(health_bar, (20, 30))

        # affichage de la barre de stamina
        font_stat.blit(stamina_bar, (20, 70))
        pygame.draw.rect(font_stat, "black", (52, 87, 101, 16))
        pygame.draw.rect(font_stat, "yellow", (55, 90, 95 * stamina_ratio, 10))

        # affichage des stats
        font = self.RPG_stats_police
        font_health = self.RPG_health_police
        text_health = f'{round(self.player.health)} / {round(self.player.max_health)}'
        text_attack = f'attaque : {round(self.player.attack, 1)}'
        text_defense = f'defense : {round(self.player.defense, 1)}'

        text_health_display = font_health.render(text_health, 1, (0, 0, 0))
        text_attack_display = font.render(text_attack, 1, (0, 0, 0))
        text_defense_display = font.render(text_defense, 1, (0, 0, 0))

        font_stat.blit(text_health_display, (health_bar.get_rect().x + 58, health_bar.get_rect().y + 43))
        font_stat.blit(text_attack_display, (25, 150))
        font_stat.blit(text_defense_display, (115, 150))

        screen.blit(font_stat, (0, 0))


    '''methode pour verifier les collisions entre un sprite et un groupe de sprite'''
    def check_collision(self, sprite, group):
        return pygame.sprite.spritecollide(sprite, group, False, pygame.sprite.collide_mask)


    '''methode pour faire apparaitre un nouvel objet monstre avec :
    un nom, des points de vie, d'attaque et de defense ainsi qu'une vitesse propre et un nombre de point d'experience qu'il donnera au joueur à sa mort'''
    def spawn_monster(self, name, health, attack, defense, velocity, xp_dropped):

        monster = Monster(self, name = name, health = health, attack = attack, defense= defense, velocity = velocity, xp_dropped= xp_dropped)
        self.all_monsters.add(monster)


    '''methode pour calculer la distance entre deux entités en utilisant pythagore'''
    def distance_entity(self, entity_1, entity_2):

        entity_1_center_x = entity_1.rect.x + entity_1.size[0] / 2
        entity_1_center_y = entity_1.rect.y + entity_1.size[1] / 2

        entity_2_center_x = entity_2.rect.x + entity_2.size[0] / 2
        entity_2_center_y = entity_2.rect.y + entity_2.size[1] / 2

        distance_entity_x = abs(entity_1_center_x - entity_2_center_x)
        distance_entity_y = abs(entity_1_center_y - entity_2_center_y)

        distance_entity = (distance_entity_x ** 2 + distance_entity_y ** 2) ** (1/2)

        return distance_entity


    '''afficher un message au milieu bas sur un fond noir'''
    def message(self, screen, text, position):
        '''Affichage d'un texte'''

        text_divided = text.split("/")

        font = pygame.image.load('assets/map/back_black.png')
        font = pygame.transform.scale(font, (len(text) * 8, len(text_divided) * 40))

        text_display = self.RPG_texte_police.render(text, 1, (255, 255, 255))

        for pos, line in enumerate(text_divided):

            text_display = self.RPG_texte_police.render(line, 1, (255, 255, 255))
            font.blit(text_display, (font.get_size()[0]/2 - text_display.get_size()[0]/2, 15 + (font.get_size()[1]/text_display.get_size()[1]) * 4.5 * pos))

        screen.blit(font, (position[0] - font.get_size()[0] // 2, position[1] - font.get_size()[1]))


    '''arreter le jeu et le reinitialiser'''
    def end_game_RPG(self):

        # reinitialisation du jeu

        # reset des statistiques du joueur
        self.player.set_level(1)

        self.player.velocity = self.player.velocity_saved


        # destruction de tous les monstres
        self.all_monsters = pygame.sprite.Group()

        self.countTimer = 0
        self.RPG_is_playing = False

        self.sound_manager.stop(self.music)
