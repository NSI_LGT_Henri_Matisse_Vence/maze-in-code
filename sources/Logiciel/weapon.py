import pygame

'''class gerant les armes'''

class Weapon(pygame.sprite.Sprite):

    def __init__(self, name, damage, couldown):
        super().__init__()

        self.name = name
        self.damage = damage
        self.couldown = couldown
        self.dic_images = {}
        self.load_images()
        self.image = self.dic_images['down']


    '''charge toutes les images d'une arme'''
    def load_images(self):
        direction = ['right', 'left', 'down', 'up']

        for direct in direction:
            image = pygame.image.load(f'assets/sprite_entity/weapons/{self.name}/{self.name}_{direct}.png')
            image = pygame.transform.scale(image, (image.get_size()[0] * 1.5, image.get_size()[1] * 1.5))
            self.dic_images[direct] = image