import pygame


'''class gerant les sons du jeu Ethernal Maze'''
class SoundManager():

    def __init__(self):
        self.sounds = {
            'click': pygame.mixer.Sound('assets/sounds/click.ogg'),
            'game_over': pygame.mixer.Sound('assets/sounds/game_over.ogg'),
            'teleportation': pygame.mixer.Sound('assets/sounds/meteorite.ogg'),
            'slash': pygame.mixer.Sound('assets/sounds/slash.ogg'),
            'death_monster': pygame.mixer.Sound('assets/sounds/death_monster.ogg'),
            'floor_1': pygame.mixer.Sound('assets/sounds/floor_1.ogg'),
            'explosion_floor_1': pygame.mixer.Sound('assets/sounds/explosion_floor_1.ogg')
        }


    '''lancer une musique'''
    def play(self, name):
        self.sounds[name].play()

    '''arreter une musique'''
    def stop(self, name):
        self.sounds[name].stop()

    '''modifier le volume sonore d'une musique'''
    def volume(self, name, volume_value):
        self.sounds[name].set_volume(volume_value)

    '''recommencer une musique'''
    def restart(self, name):
        self.sounds[name].stop()
        self.sounds[name].play()