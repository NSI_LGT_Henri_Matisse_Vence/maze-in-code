import pygame
from weapon import Weapon

#créer la class du joueur
class Player(pygame.sprite.Sprite):

    def __init__(self, game):
        super().__init__()
        self.game = game

        #stat
        self.health = 250
        self.max_health = 250
        self.health_regeneration = 0.12
        self.health_regeneration_saved = 0.12

        self.stamina = 100
        self.max_stamina = 100
        self.stamina_regeneration = 0.03

        self.attack = 1
        self.defense = 1
        self.velocity = 2

        # stat saved
        self.health_saved = 400
        self.max_health_saved = 400

        self.stamina_saved = 100
        self.max_stamina_saved = 100

        self.attack_saved = 1
        self.defense_saved = 1

        self.velocity_saved = 2

        self.couldown = 10

        #items
        self.item1 = None
        self.item2 = None
        self.item3 = None
        self.weapon_equiped = None

        #armor
        self.helmet = None
        self.chestplate = None
        self.leggings = None
        self.boots = None
        self.total_armor = None

        #xp
        self.xp = 0
        self.level = 1
        self.xp_required = 20
        self.xp_required_saved = 20

        #counter
        self.couldown_count = 0
        self.count = 0
        self.intRun = 0

        #sprites
        self.dicim = {
            "down" : pygame.image.load('assets\sprite_entity\player\warrior_m_down.png'),
            "down1" : pygame.image.load('assets\sprite_entity\player\warrior_m_down1.png'),
            "down2" : pygame.image.load('assets\sprite_entity\player\warrior_m_down2.png'),
            "up" : pygame.image.load('assets\sprite_entity\player\warrior_m_up.png'),
            "up1" : pygame.image.load('assets\sprite_entity\player\warrior_m_up1.png'),
            "up2" : pygame.image.load('assets\sprite_entity\player\warrior_m_up2.png'),
            "left": pygame.image.load('assets\sprite_entity\player\warrior_m_left.png'),
            "left1": pygame.image.load('assets\sprite_entity\player\warrior_m_left1.png'),
            "left2": pygame.image.load('assets\sprite_entity\player\warrior_m_left2.png'),
            "right": pygame.image.load('assets\sprite_entity\player\warrior_m_right.png'),
            "right1": pygame.image.load('assets\sprite_entity\player\warrior_m_right1.png'),
            "right2": pygame.image.load('assets\sprite_entity\player\warrior_m_right2.png'),
        }
        self.image = self.dicim["right"]
        self.direction = 'right'

        self.rect = self.image.get_rect()
        self.size = (self.rect[2], self.rect[3])
        self.size_saved = {
            'right': (self.dicim['right1'].get_rect()[2], self.dicim['right1'].get_rect()[3]),
            'left': (self.dicim['left1'].get_rect()[2], self.dicim['left1'].get_rect()[3]),
            'down': (self.dicim['down1'].get_rect()[2], self.dicim['down1'].get_rect()[3]),
            'up': (self.dicim['up1'].get_rect()[2], self.dicim['up1'].get_rect()[3])
            }
        self.rect.x = 0
        self.rect.y = 0


    '''methode pour mettre à jour la barre de vie'''
    def update_health_bar(self, surface):

        health_player_ratio = self.health / self.max_health

        pygame.draw.rect(surface, "black", (self.rect.x, self.rect.y - 10, self.size[0], 5))
        pygame.draw.rect(surface, "green", (self.rect.x, self.rect.y - 10, self.size[0] * health_player_ratio, 5))


    '''methode pour mettre à jour la barre d'experience'''
    def update_xp_bar(self, surface):

        xp_player_ratio = self.xp / self.xp_required

        pygame.draw.rect(surface, "black", (self.rect.x, self.rect.y - 4, self.size[0], 3))
        pygame.draw.rect(surface, "blue", (self.rect.x, self.rect.y - 4, self.size[0] * xp_player_ratio, 3))


    '''methode pour afficher le niveau du joueur'''
    def update_level_counter(self, surface):

        level_display = self.game.RPG_level_police.render(str(self.level), 1, (0, 0, 0))
        level_rect = (self.rect.x + self.size[0]/2 - level_display.get_size()[0]/2, self.rect.y - 15 - level_display.get_size()[1]/2)

        surface.blit(level_display, level_rect)


    '''methode pour appliquer un certain niveau au joueur'''
    def set_level(self, level):

        level_factor = 1.11

        self.level = level
        self.xp_required = self.xp_required_saved * (level_factor ** level)
        self.xp = 0

        level -= 1

        # reset the stats
        self.health = self.health_saved * (level_factor ** level)
        self.max_health = self.max_health_saved * (level_factor ** level)

        self.health_regeneration = self.health_regeneration_saved * (level_factor ** level)

        self.stamina = self.stamina_saved * (level_factor ** level)
        self.max_stamina = self.max_stamina_saved * (level_factor ** level)

        self.attack = self.attack_saved * (level_factor ** level)

        self.defense = self.defense_saved * (level_factor ** level)


    '''methode pour faire monter le joueur d'un niveau'''
    def new_level(self):

        while self.xp >= self.xp_required:

            self.level += 1
            self.xp -= self.xp_required

            level_factor = 1.11

            # new stat
            self.health *= level_factor
            self.max_health *= level_factor
            self.health_regeneration *= level_factor + 0.1

            self.stamina *= level_factor
            self.max_stamina *= level_factor

            self.attack *= level_factor

            self.defense *= level_factor

            self.couldown = 10

            self.xp_required *= level_factor + 0.02
            print("new level !")


    '''methode pour equiper une arme'''
    def unsheathe(self, surface, weapon_name, weapon_damage, weapon_couldown):

        self.weapon_equiped = Weapon(weapon_name, weapon_damage, weapon_couldown)
        self.weapon_equiped.image = self.weapon_equiped.dic_images[self.direction]
        self.couldown = self.weapon_equiped.couldown


        if self.direction == 'right':
            weapon_position = (self.rect.x + self.size[0]/10, self.rect.y + self.size[1]/1.5)

        elif self.direction == 'left':
            weapon_position = (self.rect.x, self.rect.y + self.size[1]/2)

        elif self.direction == 'down':
            weapon_position = (self.rect.x, self.rect.y + self.size[1]/1.35)

        elif self.direction == 'up':
            weapon_position = (self.rect.x, self.rect.y + self.size[1]/6)

        surface.blit(self.weapon_equiped.image, weapon_position)


    '''methode pour attaquer une autre entité'''
    def attack_entity(self, entity):
        if self.weapon_equiped is not None:
            self.game.sound_manager.play('slash')
            entity.damage(self.weapon_equiped.damage * (self.attack / 2))

        else:
            entity.damage(self.attack)

        if entity.health <= 0:
            self.xp += entity.xp_dropped


    '''methode pour recevoir des dégats d'une autre entité'''
    def damage(self, damage_amount):
        damage = damage_amount * ((100 - self.defense) / 100)

        if damage > 0:
            self.health -= damage


    '''methode pour changer la vitesse du joueur (pour le jeu escape)'''
    def change_velocity(self, new_velocity):
        self.velocity = new_velocity


    '''methode pour changer la taille du joueur'''
    def resize(self, coeff):
        self.image = pygame.transform.scale(self.image, (self.size_saved[self.direction][0] * coeff, self.size_saved[self.direction][1] * coeff))
        self.rect = self.image.get_rect()
        self.size = (self.rect[2], self.rect[3])


    '''methode renvoyant la vitesse du joueur'''
    def get_velocity(self):
        return self.velocity


    #mouvement

    #aller à un emplacement aux coordonnées x et y
    def goto(self, x, y):
        self.rect.x = x
        self.rect.y = y


    '''déplacer le joueur lors d'une cinématique'''
    def deplacment(self, x, y):
        if self.rect.x < x:
            self.move_right()

        if self.rect.x > y:
            self.move_left()

        if self.rect.y < y:
            self.move_down()

        if self.rect.y > y:
            self.move_up()

    # aller à droite
    def move_right(self):
        self.rect.x += self.velocity

        if self.count < 25:
            self.image = self.dicim['right1']
        elif self.count < 50:
            self.image = self.dicim['right2']
        elif self.count >= 75:
            self.image = self.dicim['right']
        else:
            self.count = 0

        self.count += 1


    # aller à gauche
    def move_left(self):
        self.rect.x -= self.velocity

        if self.count < 25:
            self.image = self.dicim['left1']
        elif self.count < 50:
            self.image = self.dicim['left2']
        elif self.count < 75:
            self.image = self.dicim['left']
            self.count = 0

        self.count += 1


    # aller en bas
    def move_down(self):
        self.rect.y += self.velocity

        if self.count < 25:
            self.image = self.dicim['down1']
        elif self.count < 50:
            self.image = self.dicim['down2']
        elif self.count > 75:
            self.image = self.dicim['down']
        else:
            self.count = 0

        self.count += 1


    #aller en haut
    def move_up(self):
        self.rect.y -= self.velocity
        if self.count < 25:
            self.image = self.dicim['up1']
        elif self.count < 50:
            self.image = self.dicim['up2']
        elif self.count < 75:
            self.image = self.dicim['up']
        else:
            self.count = 0

        self.count += 1

