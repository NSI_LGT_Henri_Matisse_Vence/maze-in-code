import pygame


#class gérant la notion de monstre

class Monster(pygame.sprite.Sprite):


    def __init__(self, game, name, health, attack, defense, velocity, xp_dropped):
        super().__init__()
        self.game = game

        self.name = name

        self.health = health
        self.max_health = health

        self.attack = attack
        self.defense = defense
        self.velocity = velocity
        self.xp_dropped = xp_dropped

        self.weapon_equiped = None

        self.dic_images = {}
        self.load_images(self.name)
        self.image = self.dic_images[name]['right3']

        self.rect = self.image.get_rect()
        self.size = (self.rect[2], self.rect[3])
        self.size_saved = {'right': (self.dic_images[name]['right1'].get_rect()[2], self.dic_images[name]['right1'].get_rect()[3]),
                          'left': (self.dic_images[name]['left1'].get_rect()[2], self.dic_images[name]['left1'].get_rect()[3]),
                          'down': (self.dic_images[name]['down1'].get_rect()[2], self.dic_images[name]['down1'].get_rect()[3]),
                          'up': (self.dic_images[name]['up1'].get_rect()[2], self.dic_images[name]['up1'].get_rect()[3])
                           }

        self.rect.x = 100
        self.rect.y = 100

        self.direction = 'right'

        # counter
        self.animation_count = 0
        self.verification_animation = True


    '''methode pour l'affichage sur une surface de la barre de vie des monstres'''
    def update_health_bar(self, surface):

        health_monster_ratio = self.health / self.max_health

        pygame.draw.rect(surface, "black", (self.rect.x, self.rect.y - 3, self.size[0], 3))
        pygame.draw.rect(surface, "red", (self.rect.x, self.rect.y - 3, self.size[0] * health_monster_ratio, 3))


    '''methode pour l'affichage du nom des monstres'''
    def pseudo(self, surface):
        font = self.game.RPG_pseudo_monster_basic_police
        text = self.name

        name_display = font.render(text, 1, (0, 0, 0))

        surface.blit(name_display, (self.rect.x + self.size[0]/2 - name_display.get_size()[0]/2, self.rect.y - 8 - name_display.get_size()[1]/2))


    '''methode pour l'action d'attaque des monstres'''
    def attack(self, entity):
        if self.weapon_equiped is not None:
            entity.damage(self.attack + self.weapon_equiped.damage)

        else:
            entity.damage(self.attack)


    '''methode pour l'action de subir des dégats des monstres'''
    def damage(self, damage_amount):

        '''les dégats subit "damage_amount" sont réduit en fonction de la defense du monstre pouvant aller de 1 à 100 :
        dégats subis = dégats infligés * ((100 - defense du monstre) / 100)'''

        damage = damage_amount * ((100 - self.defense) / 100)

        if damage > 0:
            self.health -= damage


    '''methode pour faire monter de niveau un monstre (lorsqu'il meurt) '''
    def level_up(self):

        level_factor = 1.112

        self.health *= level_factor
        self.max_health *= level_factor

        self.attack *= level_factor
        if self.defense * level_factor >= 90:
            self.defense *= level_factor

        self.xp_dropped *= level_factor


    '''methode pour placer le monstre à un endroit de coordonnées x, y'''
    def goto(self, x, y):
        self.rect.x = x
        self.rect.y = y


    '''methode pour changer la taille du monstre'''
    def resize(self, coeff):
        rect = self.rect
        self.image = pygame.transform.scale(self.image, (self.size_saved[self.direction][0] * coeff, self.size_saved[self.direction][1] * coeff))
        self.size = (self.image.get_size()[0], self.image.get_size()[1])


    '''methode pour aller à droite'''
    def right(self):

        if not self.game.check_collision(self, self.game.all_players):
            self.rect.x += self.velocity

        else:
            self.rect.x += self.velocity * 0.05
            self.game.player.health -= self.attack * 0.05

        if self.animation_count < 10:
            self.image = self.dic_images[self.name]['right1']

        elif 10 < self.animation_count < 20:
            self.image = self.dic_images[self.name]['right2']

        else:
            self.image = self.dic_images[self.name]['right3']

        if self.animation_count >= 30:
            self.animation_count = 0

        self.verification_animation = False
        self.animation_count += 1
        self.direction = 'right'


    '''methode pour aller à gauche'''
    def left(self):
        if not self.game.check_collision(self, self.game.all_players):
           self.rect.x -= self.velocity

        else:
            self.rect.x -= self.velocity * 0.05
            self.game.player.health -= self.attack * 0.05

        if self.animation_count < 10:
            self.image = self.dic_images[self.name]['left1']

        elif 10 < self.animation_count < 20:
            self.image = self.dic_images[self.name]['left2']

        else:
            self.image = self.dic_images[self.name]['left3']

        if self.animation_count >= 30:
            self.animation_count = 0

        self.verification_animation = False
        self.animation_count += 1
        self.direction = 'left'


    '''methode pour aller en bas'''
    def down(self):

        if not self.game.check_collision(self, self.game.all_players):
            self.rect.y += self.velocity

        else:
            self.rect.y += self.velocity * 0.05
            self.game.player.health -= self.attack * 0.05

        if self.verification_animation == True:

            if self.animation_count < 10:
                self.image = self.dic_images[self.name]['down1']

            elif 10 < self.animation_count < 20:
                self.image = self.dic_images[self.name]['down2']

            else:
                self.image = self.dic_images[self.name]['down3']

            if self.animation_count >= 30:
                self.animation_count = 0

            self.animation_count += 1
            self.direction = 'down'


    '''methode pour aller en haut'''
    def up(self):
        if not self.game.check_collision(self, self.game.all_players):
           self.rect.y -= self.velocity

        else:
            self.rect.y -= self.velocity * 0.05
            self.game.player.health -= self.attack * 0.05

        if self.verification_animation == True:

            if self.animation_count < 10:
                self.image = self.dic_images[self.name]['up1']

            elif 10 < self.animation_count < 20:
                self.image = self.dic_images[self.name]['up2']

            else:
                self.image = self.dic_images[self.name]['up3']

            if self.animation_count >= 30:
                self.animation_count = 0

            self.animation_count += 1
            self.direction = 'up'


    '''fonction pour charger toutes les images des monstres'''
    def load_images(self, name):

        self.dic_images[name] = {}

        direction = ['right', 'left', 'up', 'down']

        for direct in range(4):
            for position in range(3):
                self.dic_images[name][direction[direct] + str(position + 1)] = pygame.image.load(f'assets/sprite_entity/monster/{name.lower()}/{name.lower()}_{str(direction[direct]) + str(position + 1)}.png')

