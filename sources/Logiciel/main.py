'''
Project: Maze in code (Clément K; Tom P; Nolhan G; Camille FD)
file of the game escape (mainPygame)
function:
modules used: pygame, game, time, RMG_PIL_Escape
Last modif: 19/02/2024
'''

import pygame
from game import Game
from RMG_PIL import *
from RMG_PIL_jeu import *
from time import *


pygame.init()

# générer la fenetre du jeu
pygame.display.set_caption("Maze")
screen = pygame.display.set_mode((1080, 720))
icon_window = pygame.image.load("assets/logo/logo_maze.png")
pygame.display.set_icon(icon_window)

print(screen.get_size())


# arriere plan
back_1 = pygame.image.load("assets/map/background.jpg")
back_1 = pygame.transform.scale(back_1, (screen.get_size()[0], screen.get_size()[1]))

back_black = pygame.image.load("assets/map/back_black.png")
back_black = pygame.transform.scale(back_black, (screen.get_size()[0], screen.get_size()[1]))

background = back_1

#logo
logo = pygame.image.load("assets/logo/logo.png")
logo = pygame.transform.scale(logo, (logo.get_size()[0], logo.get_size()[1]))
logo_rect = logo.get_rect()
logo_rect.x = screen.get_size()[0]/2 - logo.get_size()[0]/2
logo_rect.y = screen.get_size()[1]/2 - logo.get_size()[1]/2 - 60

# bouton de jeu Escape
button_Escape = pygame.image.load("assets/button/Escape.png")
button_Escape = pygame.transform.scale(button_Escape, (button_Escape.get_size()[0]*1.5, button_Escape.get_size()[1]*1.5))
button_Escape_rect = button_Escape.get_rect()
button_Escape_rect.x = screen.get_size()[0]/2 - button_Escape.get_size()[0]/2
button_Escape_rect.y = screen.get_size()[1]/2 - button_Escape.get_size()[1] - 90

# bouton de jeu RPG
button_RPG = pygame.image.load("assets/button/RPG.png")
button_RPG = pygame.transform.scale(button_RPG, (button_RPG.get_size()[0]*1.5, button_RPG.get_size()[1]*1.5))
button_RPG_rect = button_RPG.get_rect()
button_RPG_rect.x = screen.get_size()[0]/2 + 15
button_RPG_rect.y = screen.get_size()[1]/2 - button_RPG.get_size()[1]/2

# bouton du RMG
button_RMG = pygame.image.load("assets/button/RMG.png")
button_RMG = pygame.transform.scale(button_RMG, (button_RMG.get_size()[0]*1.5, button_RMG.get_size()[1]*1.5))
button_RMG_rect = button_RMG.get_rect()
button_RMG_rect.x = screen.get_size()[0]/2 - button_RMG.get_size()[0] - 15
button_RMG_rect.y = screen.get_size()[1]/2 - button_RMG.get_size()[1]/2

game = Game()



NUMBER_SQUARES_SIDE_RMG = 50


running = True

# boucle du jeu
while running:

    #appliquer le background du jeu
    screen.blit(background, (screen.get_size()[0] / 2 - background.get_size()[0] / 2, screen.get_size()[1] / 2 - background.get_size()[1] / 2))

    #verifie si le jeu Escape est lancé
    if game.Escape_is_playing:

        #mettre le background en noir
        background = back_black

        #lancer le jeu Escape
        game.updateEscape(screen, ti, im_maze)

        #verifier et arreter le jeu si e joueur atteint la sortie
        if game.player.rect.x > dicmaze[len(dicmaze) - 1]['pos2x'] and game.player.rect.y > dicmaze[len(dicmaze) - 1]['pos1y']:

            game.Escape_is_playing = False
            ti = time() - ti
            game.countTimer = 0
            print(round(ti // 60), "min", round(ti % 60, 1), "s")
            
    elif game.RPG_is_playing:

        #lancer le jeu RPG
        game.updateRPG(screen, ti, maze, dicmaze)

    elif game.RMG_is_playing:

        game.updateRMG(screen, maze)

    else:
        # chargement de l'arriere plan
        background = back_1

        #afficher le logo puis le bouton start
        screen.blit(logo, logo_rect)

        #affichage du bouton Escape (start)
        screen.blit(button_Escape, button_Escape_rect)

        #affichage du bouton RPG (RPG)
        screen.blit(button_RPG, button_RPG_rect)

        # affichage du bouton RMG (RMG)
        screen.blit(button_RMG, button_RMG_rect)


    # mettre à jour l'écran
    pygame.display.flip()

    #detecter les touches
    for event in pygame.event.get():

        # verif que l'evenement est fermeture de la fenetre
        if event.type == pygame.QUIT:
            running = False
            pygame.quit()

        # detect si une touche est touchée
        elif event.type == pygame.KEYDOWN:
            game.pressed[event.key] = True

        # detect si une touche est lachée
        elif event.type == pygame.KEYUP:
            game.pressed[event.key] = False

        #verifier l'action de la sourie
        elif event.type == pygame.MOUSEBUTTONDOWN:

            #actionner le bouton pour initialiser le jeu
            if not game.Escape_is_playing and not game.RPG_is_playing and not game.RMG_is_playing:

                if button_Escape_rect.collidepoint(event.pos):

                    game.sound_manager.play('click')

                    print("Escape en route")
                    pygame.display.set_caption("Escape")
                    game.message(screen, "Chargement en cours merci de patienter... /Ne rien toucher", (screen.get_size()[0]/2, screen.get_size()[1]/2))
                    pygame.display.flip()
                    
                    #mettre le jeu en route et initialiser le timer
                    game.Escape_is_playing = True
                    game.occ = 0
                    ti = time()

                    # génération du labyrinthe
                    maze = maze_f(50, 0, 600, 12, 'assets/map/maps/map_white.png', screen)
                    maze_background = pygame.image.load('assets/map/maps/map_white.png')
                    maze_background = pygame.transform.scale(maze_background, (700, 700))
                    url_maze = maze[0]
                    dicmaze = maze[1]
                    im_maze = pygame.image.load(url_maze)

                    # correction des coordonnées
                    for i in range(len(dicmaze)):
                        dicmaze[i]['x'] += 300 + screen.get_size()[0] / 2 - maze_background.get_size()[0] / 2 + 42
                        dicmaze[i]['y'] = -dicmaze[i]['y'] + 300 + screen.get_size()[1] / 2 - maze_background.get_size()[1] / 2 + 34
                        dicmaze[i]['pos1x'] += 300 + screen.get_size()[0] / 2 - maze_background.get_size()[0] / 2 + 42
                        dicmaze[i]['pos1y'] = -dicmaze[i]['y'] + 300 + screen.get_size()[1] / 2 - maze_background.get_size()[1] / 2 + 34
                        dicmaze[i]['pos2x'] += 300 + screen.get_size()[0] / 2 - maze_background.get_size()[0] / 2 + 42
                        dicmaze[i]['pos2y'] = -dicmaze[i]['y'] + 300 + screen.get_size()[1] / 2 - maze_background.get_size()[1] / 2 + 34

                    # redimensionnement du joueur
                    game.player.resize(0.7)

                    #repositionnement du joueur au début
                    game.player.goto(dicmaze[0]['x'], dicmaze[0]['y'])

                    print(dicmaze[143]['pos1x'], dicmaze[143]['pos2y'])
                    
                elif button_RPG_rect.collidepoint(event.pos):

                    game.sound_manager.play('click')

                    print("Rpg en route")
                    pygame.display.set_caption("Ethernal Maze")
                    game.message(screen, "Chargement en cours merci de patienter... /Ne rien toucher", (screen.get_size()[0]/2, screen.get_size()[1]/2))
                    pygame.display.flip()

                    #mettre le jeu en route et l'initialiser
                    game.RPG_is_playing = True
                    game.RPG_cin1_is_playing = True
                    game.occ = 0
                    ti = time()

                    # mettre le background en noir
                    background = back_black

                    
                    # génération du labyrinthe
                    maze_size = 2200
                    maze = maze_f(70, 2, maze_size, 10, 'assets/map/maps/map_floor1.png', screen)
                    print("check")
                    url_maze = maze[0]
                    dicmaze = maze[1]

                    # correction des coordonnées
                    for i in range(len(dicmaze)):
                        dicmaze[i]['x'] += maze_size//2
                        dicmaze[i]['y'] = -dicmaze[i]['y'] + maze_size//2
                        dicmaze[i]['pos1x'] += maze_size//2
                        dicmaze[i]['pos1y'] = -dicmaze[i]['pos1y'] + maze_size//2
                        dicmaze[i]['pos2x'] += maze_size//2
                        dicmaze[i]['pos2y'] = -dicmaze[i]['pos2y'] + maze_size//2


                    # application et centrage de l'image du labyrinthe au jeu
                    game.map_RPG.change_map(pygame.image.load('assets/map/maps/map_spawn.png'))
                    game.map_RPG.goto(0, 0)
                    game.map_RPG.save_rect = game.map_RPG.image.get_rect()

                    #placer le joueur
                    # redimensionnement du joueur
                    game.player.resize(2)


                elif button_RMG_rect.collidepoint(event.pos):

                    game.sound_manager.play('click')

                    print("RMG en marche")
                    game.message(screen, "Chargement en cours merci de patienter... /Ne rien toucher", (screen.get_size()[0]/2, screen.get_size()[1]/2))
                    pygame.display.flip()

                    game.RMG_is_playing = True
                    maze_size = 600
                    maze = maze_f_normal(0, maze_size, NUMBER_SQUARES_SIDE_RMG)

                    game.RMG_maze_map = pygame.image.load(maze[0])
                    game.RMG_maze = maze


        # arreter la partie si on appuie sur t
        elif game.pressed.get(pygame.K_t):
            pygame.display.set_caption("Maze")
            game.Escape_is_playing = False

            if game.RPG_is_playing:
                game.end_game_RPG()
            game.RPG_is_playing = False
            game.RPG_cin1_is_playing = False
            game.RPG_floor1_is_playing = False

            game.RMG_is_playing = False
            game.mazesolved_done = False