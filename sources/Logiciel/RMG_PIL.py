'''
Project: Maze in code (Clément K; Tom P; Nolhan G; Camille FD)
Random maze generator (RMG_PIL_V3)
function: clear() , draw() , square() , grid() , maze_f()
modules used: PIL (Image) and random (as r)
Last modif: 6/01/2024
'''

from time import *
from PIL import Image
import random as r


'''effacer l'image'''
def clear(im):


    pixels = im.load()
    for i in range(im.size[0]):
        for k in range(im.size[1]):
            pixels[i, k] = (250, 250, 250)


'''dessiner un rectangle d'un point A(x1, y1) jusqu'à un point B(x2, y2)'''
def draw(map, hub, colo, width, x1, y1, x2, y2):

    pixels = map.load()
    col = [(250, 250, 250), (0, 0, 0), (250, 0, 0), (0, 250, 0), (0, 0, 250)]
    y1, y2 = -y1, -y2

    if colo == 0:
        if x1 == x2:
            x1 -= width/2 + 1/2
            x2 += width/2 + 1/2
            y1 += width/2
            y2 -= width/2

        elif y1 == y2:
            y1 -= width/2 + 1/2
            y2 += width/2 + 1/2
            x1 += width/2
            x2 -= width/2
    else:
        if x1 == x2:
            x1 -= width / 2 + 1 / 2
            x2 += width / 2 + 1 / 2
            y1 += width / 2 + 1 / 2
            y2 -= width / 2 + 1 / 2

        elif y1 == y2:
            y1 -= width / 2 + 1 / 2
            y2 += width / 2 + 1 / 2
            x1 += width / 2 + 1 / 2
            x2 -= width / 2 + 1 / 2

    for i in range(round(x1), round(x2 + 1)):
        for j in range(round(y1), round(y2 + 1)):
            pixels[round(i + hub[0]), round(j + hub[1])] = col[colo]


'''dessiner un carré'''
def square(map, hub, width, n):
    '''Build a square (width = width, n = lenght of the square in pixel)'''

    draw(map, hub, 1, width - 1, -n/2 - 1, n/2, n/2 + 1, n/2)
    draw(map, hub, 1, width - 1, n/2 + 1, n/2 + 1, n/2 + 1, -n/2 - 1)
    draw(map, hub, 1, width - 1, -n/2 - 1, -n/2 - 1, n/2 + 1, -n/2 - 1)
    draw(map, hub, 1, width - 1, -n/2, n/2 + 1, -n/2, -n/2 - 1)


'''dessiner une grille'''
def grid(map, hub, width, n, p):
    '''Creation of a Grid (width = width of the lines, n = lenght of the lines in pixel, p = nbr of cases)'''

    square(map, hub, 2, n)
    '''Grid colones'''
    for i in range(1, p):
        draw(map, hub, 1, width, -n/2 + i*(n/p), n/2, -n/2 + i*(n/p), -n/2)
    '''Grid lines'''
    for i in range(1, p):
        draw(map, hub, 1, width, -n/2, n/2 - i*(n/p), n//2, n/2 - i*(n/p))


'''dessiner un labyrinthe: width est l'epaisseur du trait ; n la taille en pixel de l'image et p le nombre de case d'un coté'''
def maze_f_normal(width: int, n: int, p: int):

    '''Creation of the maze'''

    im = Image.new('RGB', (n + 200, n + 200), 'white')
    hub = (im.size[0] / 2, im.size[1] / 2)
    maze = {}
    start = time()
    j = 0
    nbr_case = p**2
    clear(im)
    grid(im, hub, width/2, n, p)

    '''initialize the "maze" dict containing all the boxes'''

    '''for the line'''
    for k in range(p):
        '''for the colone'''
        for i in range(p):
            '''
            case n° = number of the case
            pos1x = x coordinate of top left corner
            pos1y = y coordinate of top left corner
            pos2x = x coordinate of bottom right corner
            pos2y = y coordinate of bottom right corner
            x = x coordinate of the center of the case
            y = y coordinate of the center of the case
            /after
            '''
            maze[j] = {'case n°': j + 1,
                       'pos1x': (-n / 2 + i * n / p),
                       'pos1y': (n / 2 - k * n / p),
                       'pos2x': (-n / 2 + (i + 1) * n / p),
                       'pos2y': (n / 2 - (k + 1) * n / p),
                       'x': ((-n / 2 + i * n / p) + (-n / 2 + (i + 1) * n / p)) / 2,
                       'y': ((n / 2 - k * n / p) + (n / 2 - (k + 1) * n / p)) / 2,
                       'nbr_pass': 0,
                       'nbr_way': 0,
                       'own_nbr': j,
                       }
            j += 1

    rdm_case = 0
    rdm_nbr = r.randint(1, 4)
    all = 1

    '''while loop to make the maze's way by breaking down walls'''
    while all < nbr_case:

        rdm_own = maze[rdm_case]['own_nbr']
        count = 0

        if rdm_nbr == 1:
            if maze.get(rdm_case - 1) is not None and maze[rdm_case - 1]['own_nbr'] != maze[rdm_case]['own_nbr'] and \
                    maze[rdm_case]['pos1x'] == maze[rdm_case - 1]['pos2x']:
                draw(im, hub, 0, 1, maze[rdm_case]['pos1x'], maze[rdm_case]['pos1y'] - 1, maze[rdm_case]['pos1x'],
                       maze[rdm_case]['pos2y'] + 1)
                maze[rdm_case - 1]['own_nbr'] = maze[rdm_case]['own_nbr']
                rdm_case = rdm_case - 1
                all += 1
            else:
                rdm_nbr = r.randint(1, 4)
                count += 1

        if rdm_nbr == 2:
            if maze.get(rdm_case - p) is not None and maze[rdm_case - p]['own_nbr'] != maze[rdm_case]['own_nbr']:
                draw(im, hub, 0, 1, maze[rdm_case]['pos1x'] + 1, maze[rdm_case]['pos1y'], maze[rdm_case - p]['pos2x'] - 1,
                       maze[rdm_case - p]['pos2y'])
                maze[rdm_case - p]['own_nbr'] = maze[rdm_case]['own_nbr']
                rdm_case = rdm_case - p
                all += 1
            else:
                rdm_nbr = r.randint(1, 4)
                count += 1

        if rdm_nbr == 3:
            if maze.get(rdm_case + 1) is not None and maze[rdm_case + 1]['own_nbr'] != maze[rdm_case]['own_nbr'] and \
                    maze[rdm_case]['pos2x'] == maze[rdm_case + 1]['pos1x']:
                draw(im, hub, 0, 1, maze[rdm_case + 1]['pos1x'], maze[rdm_case + 1]['pos1y'] -1, maze[rdm_case]['pos2x'],
                       maze[rdm_case]['pos2y'] + 1)
                maze[rdm_case + 1]['own_nbr'] = maze[rdm_case]['own_nbr']
                rdm_case = rdm_case + 1
                all += 1
            else:
                rdm_nbr = r.randint(1, 4)
                count += 1

        if rdm_nbr == 4:
            if maze.get(rdm_case + p) is not None and maze[rdm_case + p]['own_nbr'] != maze[rdm_case]['own_nbr']:
                draw(im, hub, 0, 1, maze[rdm_case + p]['pos1x'] + 1, maze[rdm_case + p]['pos1y'], maze[rdm_case]['pos2x'] - 1,
                       maze[rdm_case]['pos2y'])
                maze[rdm_case + p]['own_nbr'] = maze[rdm_case]['own_nbr']
                rdm_case = rdm_case + p
                all += 1

            elif count == 3:
                al = r.randint(0, len(maze) - 1)
                while maze[al]['own_nbr'] != maze[rdm_case]['own_nbr']:
                    al = r.randint(0, len(maze) - 1)
                rdm_case = al
                maze[rdm_case]['own_nbr'] = rdm_own

            else:
                rdm_nbr = r.randint(1, 3)
                continue
        rdm_nbr = r.randint(1, 4)

    square(im, hub, width, n)
    draw(im, hub, 0, width + 1, maze[0]['pos1x'], maze[0]['pos1y'] - 2, maze[0]['pos1x'], maze[0]['pos2y'])
    draw(im, hub, 0, width + 1, maze[nbr_case - 1]['pos2x'] + 1, maze[nbr_case - 1]['pos1y'] - 2, maze[nbr_case - 1]['pos2x'] + 1, maze[nbr_case - 1]['pos2y'])

    path = f'Maze_{str(p)}_Temps_{str(round(time() - start, 3))}_{str(r.randint(0, 1000))}.png'

    im.save(path, "PNG")

    return (path, maze, im)

'''maze_f(0, 3300, 50)'''