import pygame

#créer la class du joueur
class Map(pygame.sprite.Sprite):

    def __init__(self, game):
        super().__init__()
        self.game = game

        self.health = 100
        self.max_health = 100
        self.attack = 10
        self.velocity = 3
        self.min_velocity = 3
        self.max_velocity = 5
        
        self.item1 = None
        self.item2 = None
        self.item3 = None
        
        self.count = 0
        self.intRun = 0
        self.image = pygame.image.load("assets/sprite_entity/player/warrior_m_right.png")
        self.dicim = {}
        self.rect = self.image.get_rect()
        self.save_rect = self.image.get_rect()
        self.size = (self.rect[2], self.rect[3])
        self.rect.x = 0
        self.rect.y = 0


    #centrer la map
    def center(self, screen, maze):
        self.rect.x = screen.get_size()[0]/2 - maze[3]/2 - 50
        self.rect.y = screen.get_size()[1]/2 - maze[3]/2 - 50

    #changer de map et enregistrer ses dimensions et coordonnées
    def change_map(self, map):
        self.image = map
        self.rect = self.image.get_rect()
        self.size = (self.rect[2], self.rect[3])

    #changer la vitesse de la map
    def change_velocity(self, new_velocity):
        self.velocity = new_velocity

    #obtenir la vitesse de la map
    def get_velocity(self):
        return self.velocity


    #mouvement

    # aller à un emplacement aux coordonnées x et y
    def goto(self, x, y):
        self.rect.x = x
        self.rect.y = y

    #aller à droite
    def move_right(self):
        if not self.game.check_collision(self.game.player, self.game.all_monsters):
            self.rect.x -= self.velocity

            for monster in self.game.all_monsters:
                monster.rect.x -= self.velocity

        else:
            self.rect.x -= self.velocity * 0.6

            for monster in self.game.all_monsters:
                monster.rect.x -= self.velocity * 0.6

        if self.game.player.count < 25:
            self.game.player.image = self.game.player.dicim['right1']

        else:
            self.game.player.image = self.game.player.dicim['right2']

        if self.game.player.count >= 50:
            self.game.player.count = 0

        self.game.player.direction = 'right'
        self.game.player.count += 1


    #... à gauche
    def move_left(self):

        if not self.game.check_collision(self.game.player, self.game.all_monsters):
            self.rect.x += self.velocity

            for monster in self.game.all_monsters:
                monster.rect.x += self.velocity

        else:
            self.rect.x += self.velocity * 0.6

            for monster in self.game.all_monsters:
                monster.rect.x += self.velocity * 0.6

        if self.game.player.count < 25:
            self.game.player.image = self.game.player.dicim['left1']

        else:
            self.game.player.image = self.game.player.dicim['left2']

        if self.game.player.count >= 50:
            self.game.player.count = 0

        self.game.player.direction = 'left'
        self.game.player.count += 1


    #... en bas
    def move_down(self):

        if not self.game.check_collision(self.game.player, self.game.all_monsters):
            self.rect.y -= self.velocity

            for monster in self.game.all_monsters:
                monster.rect.y -= self.velocity

        else:
            self.rect.y -= self.velocity * 0.6

            for monster in self.game.all_monsters:
                monster.rect.y -= self.velocity * 0.6

        if self.game.player.count < 25:
            self.game.player.image = self.game.player.dicim['down1']

        else:
            self.game.player.image = self.game.player.dicim['down2']

        if self.game.player.count >= 50:
            self.game.player.count = 0

        self.game.player.direction = 'down'
        self.game.player.count += 1

    #... en haut
    def move_up(self):

        if not self.game.check_collision(self.game.player, self.game.all_monsters):
           self.rect.y += self.velocity

           for monster in self.game.all_monsters:
               monster.rect.y += self.velocity

        else:
            self.rect.y += self.velocity * 0.6

            for monster in self.game.all_monsters:
                monster.rect.y += self.velocity * 0.6

        if self.game.player.count < 25:
            self.game.player.image = self.game.player.dicim['up1']

        else:
            self.game.player.image = self.game.player.dicim['up2']

        if self.game.player.count >= 50:
            self.game.player.count = 0

        self.game.player.direction = 'up'
        self.game.player.count += 1