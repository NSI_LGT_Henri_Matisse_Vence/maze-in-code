'''
Project: Maze in Code (Clément K; Tom P; Nolhan G; Camille FD)
Random Maze Resolver (PIL)
function: maze_res_f()
modules used: RMR_V1.2 (with RMG_PIL_V3)
Last modif: 06/01/2024
'''


from RMG_PIL import *


'''fonction pour résoudre un labyrinthe'''
def maze_res_f(map):
    start = time()
    m_map = map[0]
    maze = map[1]
    pixels = m_map.load()
    hub = (m_map.size[0] / 2, m_map.size[1] / 2)
    l_case = 0
    case = 0
    col = 2
    if len(maze)**(1/2) == 150:
        wd = -1
    elif len(maze)**(1/2) >= 40:
        wd = 0
    else:
        wd = 1
    p = len(maze)**(1/2)

    maze[0]['nbr_pass'] = 1
    draw(m_map, hub, col, wd, maze[0]['pos1x'], maze[0]['y'], maze[0]['x'], maze[0]['y'])
    while case != len(maze) - 1:

        col = 2

        if l_case == case or l_case == case - 1: #droite

            if maze.get(case + p) is not None and pixels[maze[case]['x'] + hub[0], - maze[case]['pos2y'] + hub[1]] != (0, 0, 0):

                l_case, case = case, case + p
                if pixels[maze[case]['x'] + hub[0], -maze[case]['y'] + hub[1]] != (250, 250, 250):
                    draw(m_map, hub, 0, wd, maze[l_case]['x'], maze[l_case]['y'], maze[case]['x'], maze[case]['y'] - (wd + 1))
                else:
                    draw(m_map, hub, col, wd, maze[l_case]['x'], maze[l_case]['y'] + (wd + 1), maze[case]['x'], maze[case]['y'])

            elif maze.get(case + 1) is not None and pixels[maze[case]['pos2x'] + hub[0], - maze[case]['y'] + hub[1]] != (0, 0, 0):

                l_case, case = case, case + 1
                if pixels[maze[case]['x'] + hub[0], -maze[case]['y'] + hub[1]] != (250, 250, 250):
                    draw(m_map, hub, 0, wd, maze[l_case]['x'], maze[l_case]['y'], maze[case]['x'] + (wd + 1), maze[case]['y'])
                else:
                    draw(m_map, hub, col, wd, maze[l_case]['x'] - (wd + 1), maze[l_case]['y'], maze[case]['x'], maze[case]['y'])

            elif maze.get(case - p) is not None and pixels[maze[case]['x'] + hub[0], - maze[case]['pos1y'] + hub[1]] != (0, 0, 0):

                l_case, case = case, case - p
                if pixels[maze[case]['x'] + hub[0], -maze[case]['y'] + hub[1]] != (250, 250, 250):
                    draw(m_map, hub, 0, wd, maze[case]['x'], maze[case]['y'] + (wd + 1), maze[l_case]['x'], maze[l_case]['y'])
                else:
                    draw(m_map, hub, col, wd, maze[case]['x'], maze[case]['y'], maze[l_case]['x'], maze[l_case]['y'] - (wd + 1))

            else:
                col = 0
                case, l_case = l_case, case
                draw(m_map, hub, col, wd, maze[case]['x'] - (wd + 1), maze[case]['y'], maze[l_case]['x'], maze[l_case]['y'])

        elif l_case == case - p: #descente

            if maze.get(case - 1) is not None and pixels[maze[case]['pos1x'] + hub[0], - maze[case]['y'] + hub[1]] != (0, 0, 0):

                l_case, case = case, case - 1
                if pixels[maze[case]['x'] + hub[0], -maze[case]['y'] + hub[1]] != (250, 250, 250):
                    draw(m_map, hub, 0, wd, maze[case]['x'] - (wd + 1), maze[case]['y'], maze[l_case]['x'], maze[l_case]['y'])
                else:
                    draw(m_map, hub, col, wd, maze[case]['x'], maze[case]['y'], maze[l_case]['x'] + (wd + 1), maze[l_case]['y'])

            elif maze.get(case + p) is not None and pixels[maze[case]['x'] + hub[0], - maze[case]['pos2y'] + hub[1]] != (0, 0, 0):

                l_case, case = case, case + p
                if pixels[maze[case]['x'] + hub[0], -maze[case]['y'] + hub[1]] != (250, 250, 250):
                    draw(m_map, hub, 0, wd, maze[l_case]['x'], maze[l_case]['y'], maze[case]['x'], maze[case]['y'] - (wd + 1))
                else:
                    draw(m_map, hub, col, wd, maze[l_case]['x'], maze[l_case]['y'] + (wd + 1), maze[case]['x'], maze[case]['y'])

            elif maze.get(case + 1) is not None and pixels[maze[case]['pos2x'] + hub[0], - maze[case]['y'] + hub[1]] != (0, 0, 0):

                l_case, case = case, case + 1
                if pixels[maze[case]['x'] + hub[0], -maze[case]['y'] + hub[1]] != (250, 250, 250):
                    draw(m_map, hub, 0, wd, maze[l_case]['x'], maze[l_case]['y'], maze[case]['x'] + (wd + 1), maze[case]['y'])
                else:
                    draw(m_map, hub, col, wd, maze[l_case]['x'] - (wd + 1), maze[l_case]['y'], maze[case]['x'], maze[case]['y'])

            else:
                col = 0
                case, l_case = l_case, case
                draw(m_map, hub, col, wd, maze[case]['x'], maze[case]['y'] + (wd + 1), maze[l_case]['x'], maze[l_case]['y'])

        elif l_case == case + 1: #gauche

            if maze.get(case - p) is not None and pixels[maze[case]['x'] + hub[0], - maze[case]['pos1y'] + hub[1]] != (0, 0, 0):

                l_case, case = case, case - p
                if pixels[maze[case]['x'] + hub[0], -maze[case]['y'] + hub[1]] != (250, 250, 250):
                    draw(m_map, hub, 0, wd, maze[case]['x'], maze[case]['y'] + (wd + 1), maze[l_case]['x'], maze[l_case]['y'])
                else:
                    draw(m_map, hub, col, wd, maze[case]['x'], maze[case]['y'] , maze[l_case]['x'], maze[l_case]['y'] - (wd + 1))

            elif maze.get(case - 1) is not None and pixels[maze[case]['pos1x'] + hub[0], - maze[case]['y'] + hub[1]] != (0, 0, 0):

                l_case, case = case, case - 1
                if pixels[maze[case]['x'] + hub[0], -maze[case]['y'] + hub[1]] != (250, 250, 250):
                    draw(m_map, hub, 0, wd, maze[case]['x'] - (wd + 1), maze[case]['y'], maze[l_case]['x'], maze[l_case]['y'])
                else:
                    draw(m_map, hub, col, wd, maze[case]['x'], maze[case]['y'], maze[l_case]['x'] + (wd + 1), maze[l_case]['y'])

            elif maze.get(case + p) is not None and pixels[maze[case]['x'] + hub[0], - maze[case]['pos2y'] + hub[1]] != (0, 0, 0):

                l_case, case = case, case + p
                if pixels[maze[case]['x'] + hub[0], -maze[case]['y'] + hub[1]] != (250, 250, 250):
                    draw(m_map, hub, 0, wd, maze[l_case]['x'], maze[l_case]['y'], maze[case]['x'], maze[case]['y'] - (wd + 1))
                else:
                    draw(m_map, hub, col, wd, maze[l_case]['x'], maze[l_case]['y'] + (wd + 1), maze[case]['x'], maze[case]['y'])

            else:
                col = 0
                case, l_case = l_case, case
                draw(m_map, hub, col, wd, maze[l_case]['x'], maze[l_case]['y'], maze[case]['x'] + (wd + 1), maze[case]['y'])

        elif l_case == case + p: #montée

            if maze.get(case + 1) is not None and pixels[maze[case]['pos2x'] + hub[0], - maze[case]['y'] + hub[1]] != (0, 0, 0):

                l_case, case = case, case + 1
                if pixels[maze[case]['x'] + hub[0], -maze[case]['y'] + hub[1]] != (250, 250, 250):
                    draw(m_map, hub, 0, wd, maze[l_case]['x'], maze[l_case]['y'], maze[case]['x'] + (wd + 1), maze[case]['y'])
                else:
                    draw(m_map, hub, col, wd, maze[l_case]['x'] - (wd + 1), maze[l_case]['y'], maze[case]['x'], maze[case]['y'])

            elif maze.get(case - p) is not None and pixels[maze[case]['x'] + hub[0], - maze[case]['pos1y'] + hub[1]] != (0, 0, 0):

                l_case, case = case, case - p
                if pixels[maze[case]['x'] + hub[0], -maze[case]['y'] + hub[1]] != (250, 250, 250):
                    draw(m_map, hub, 0, wd, maze[case]['x'], maze[case]['y'] + (wd + 1), maze[l_case]['x'], maze[l_case]['y'])
                else:
                    draw(m_map, hub, col, wd, maze[case]['x'], maze[case]['y'], maze[l_case]['x'], maze[l_case]['y'] - (wd + 1))

            elif maze.get(case - 1) is not None and pixels[maze[case]['pos1x'] + hub[0], - maze[case]['y'] + hub[1]] != (0, 0, 0):

                l_case, case = case, case - 1
                if pixels[maze[case]['x'] + hub[0], -maze[case]['y'] + hub[1]] != (250, 250, 250):
                    draw(m_map, hub, 0, wd, maze[case]['x'] - (wd + 1), maze[case]['y'], maze[l_case]['x'], maze[l_case]['y'])
                else:
                    draw(m_map, hub, col, wd, maze[case]['x'], maze[case]['y'], maze[l_case]['x'] + (wd + 1), maze[l_case]['y'])

            else:
                col = 0
                case, l_case = l_case, case
                draw(m_map, hub, col, wd, maze[l_case]['x'], maze[l_case]['y'], maze[case]['x'], maze[case]['y'] - (wd + 1))

        maze[case]['nbr_pass'] += 1

    draw(m_map, hub, col, wd, maze[len(maze) - 1]['x'], maze[len(maze) - 1]['y'], maze[len(maze) - 1]['pos2x'], maze[len(maze) - 1]['y'])

    path = f'MazeZ_{str(p)}_{str(r.randint(-1000, -1))}.png'

    m_map.save(path, "PNG")
    print("Résolution time :", time() - start)

    return path