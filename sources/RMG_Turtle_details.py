'''
Project: Maze in code (Clément K; Tom P; Nolhan G; Camille FD)
Random maze generator
function: erased() , entrance() and maze_function()
modules used: turtle (as t) and random (as r)
Last modif: 16/11/2023
'''

import turtle as t
import random as r

t.width(1)
t.shape("classic")
t.speed("fastest")
t.fillcolor('Blue')
maze = {}
nbr_case = 0
n = 500
spawn = (-n / 2, n / 2)

'''function erased:
erase a wall of the grid using its coordonnate
'''

def erased(width, x1, y1, x2, y2):
    if x1 == x2:
        y1 -= 1
    else:
        x1 += 1
    t.width(width)
    t.up()
    t.goto(x1, y1)
    t.pencolor('white')
    t.down()
    t.goto(x2, y2)
    t.up()
    t.pencolor('black')
    t.width(1)

'''Make the entrance and the exit of the maze :'''

def entrance(p):
    global spawn
    t.up()
    t.goto(spawn)
    t.width(3)
    t.down()
    t.pencolor('black')

    '''build the square'''

    for i in range(4):
        t.forward(n)
        t.right(90)

    '''remake the entrance and the exit'''

    '''entrance'''
    erased(3, spawn[0], spawn[1], -n / 2, n / 2 - n / p)

    '''exit'''
    erased(3, -spawn[0], -spawn[1], n / 2, -n / 2 + n / p)


'''maze_function:
build a square maze using the parameter p as length of one side
'''

def maze_function(p):
    global n
    nbr_case = p ** 2
    j = 0

    entrance(p)

    '''make the grid:'''

    '''grid colone'''
    for i in range(1, p):
        t.up()
        t.goto(-n / 2 + i * (n / p), n / 2)
        t.down()
        t.goto(-n / 2 + i * (n / p), -n / 2)

    '''grid line'''
    for i in range(1, p):
        t.up()
        t.goto(-n / 2, n / 2 - i * (n / p))
        t.down()
        t.goto(n / 2, n / 2 - i * (n / p))

    '''initialize the "maze" dict containing all the boxes'''

    '''for the line'''
    for k in range(p):
        '''for the colone'''
        for i in range(p):
            '''
            case n° = number of the case
            pos1x = x coordinate of top left corner
            pos1y = y coordinate of top left corner
            pos2x = x coordinate of bottom right corner
            pos2y = y coordinate of bottom right corner
            x = x coordinate of the center of the case
            y = y coordinate of the center of the case
            /after
            '''
            maze[j] = {'case n°': j + 1,
                       'pos1x': (-n / 2 + i * n / p),
                       'pos1y': (n / 2 - k * n / p),
                       'pos2x': (-n / 2 + (i + 1) * n / p),
                       'pos2y': (n / 2 - (k + 1) * n / p),
                       'x': ((-n / 2 + i * n / p) + (-n / 2 + (i + 1) * n / p)) / 2,
                       'y': ((n / 2 - k * n / p) + (n / 2 - (k + 1) * n / p)) / 2,
                       'nbr_pass': 0,
                       'nbr_way': 0,
                       'own_nbr': j,
                       }
            j += 1

    rdm_case = 0
    rdm_nbr = r.randint(1, 4)
    all = 0
    t.speed('fastest')


    """while loop to make the maze's way by breaking down walls """

    while all < nbr_case:
        all = 0
        for i in range(nbr_case):
            if maze[0]['own_nbr'] == maze[i]['own_nbr']:
                all += 1
        rdm_own = maze[rdm_case]['own_nbr']
        count = 0

        if rdm_nbr == 1:
            if maze.get(rdm_case -1) is not None and maze[rdm_case - 1]['own_nbr'] != maze[rdm_case]['own_nbr'] and maze[rdm_case]['pos1x'] == maze[rdm_case - 1]['pos2x']:
                erased(1, maze[rdm_case]['pos1x'], maze[rdm_case]['pos1y'], maze[rdm_case]['pos1x'],
                       maze[rdm_case]['pos2y'])
                maze[rdm_case - 1]['own_nbr'] = maze[rdm_case]['own_nbr']
                rdm_case = rdm_case - 1
            else:
                rdm_nbr = r.randint(1, 4)
                count += 1

        if rdm_nbr == 2:
            if maze.get(rdm_case - p) is not None and maze[rdm_case - p]['own_nbr'] != maze[rdm_case]['own_nbr']:
                erased(1, maze[rdm_case]['pos1x'], maze[rdm_case]['pos1y'], maze[rdm_case - p]['pos2x'],
                       maze[rdm_case - p]['pos2y'])
                maze[rdm_case - p]['own_nbr'] = maze[rdm_case]['own_nbr']
                rdm_case = rdm_case - p
            else:
                rdm_nbr = r.randint(1, 4)
                count += 1

        if rdm_nbr == 3:
            if maze.get(rdm_case + 1) is not None and maze[rdm_case + 1]['own_nbr'] != maze[rdm_case]['own_nbr'] and \
                    maze[rdm_case]['pos2x'] == maze[rdm_case + 1]['pos1x']:
                erased(1, maze[rdm_case + 1]['pos1x'], maze[rdm_case + 1]['pos1y'], maze[rdm_case]['pos2x'],
                       maze[rdm_case]['pos2y'])
                maze[rdm_case + 1]['own_nbr'] = maze[rdm_case]['own_nbr']
                rdm_case = rdm_case + 1
            else:
                rdm_nbr = r.randint(1, 4)
                count += 1

        if rdm_nbr == 4:
            if maze.get(rdm_case + p) is not None and maze[rdm_case + p]['own_nbr'] != maze[rdm_case]['own_nbr']:
                erased(1, maze[rdm_case + p]['pos1x'], maze[rdm_case + p]['pos1y'], maze[rdm_case]['pos2x'],
                       maze[rdm_case]['pos2y'])
                maze[rdm_case + p]['own_nbr'] = maze[rdm_case]['own_nbr']
                rdm_case = rdm_case + p

            elif count == 3:
                al = r.randint(0, len(maze) - 1)
                while maze[al]['own_nbr'] != maze[rdm_case]['own_nbr']:
                    al = r.randint(0, len(maze) - 1)
                rdm_case = al
                maze[rdm_case]['own_nbr'] = rdm_own
            else:
                rdm_nbr = r.randint(1, 3)
                continue
        rdm_nbr = r.randint(1, 4)

    entrance(p)

    t.mainloop()

maze_function(50)