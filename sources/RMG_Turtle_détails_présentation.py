'''
Project: Maze in code (Clément K; Tom P; Nolhan G; Camille FD)
Random maze generator
function: erased() , entrance() and maze_function()
modules used: turtle (as t) and random (as r)
Last modif: 16/11/2023
'''

import keyboard
import turtle as t
import random as r

t.width(1)
t.shape("classic")
t.speed("normal")
t.fillcolor('Blue')
maze = {}
nbr_case = 0
size_maze = 500
spawn = (-size_maze / 2, size_maze / 2)

'''function erased:
erase a wall of the grid using its coordonnates
'''

def next():
    while True:
        if keyboard.read_key() == "s":
            break

def erased(width, x1, y1, x2, y2):
    if x1 == x2:
        y1 -= 1
    else:
        x1 += 1
    t.width(width)
    t.up()
    t.goto(x1, y1)
    t.pencolor('white')
    t.down()
    t.goto(x2, y2)
    t.up()
    t.pencolor('black')
    t.width(1)

'''Make the entrance and the exit of the maze :'''

def entrance(p):
    global spawn
    t.up()
    t.goto(spawn)
    t.width(3)
    t.down()
    t.pencolor('black')

    '''build the square'''

    for i in range(4):
        t.forward(size_maze)
        t.right(90)

    '''remake the entrance and the exit'''

    '''entrance'''
    erased(3, spawn[0], spawn[1], -size_maze / 2, size_maze / 2 - size_maze / p)

    '''exit'''
    erased(3, -spawn[0], -spawn[1], size_maze / 2, -size_maze / 2 + size_maze / p)
    

'''maze_function:
build a square maze using the parameter p as length of one side
'''

def maze_function(nbr_case_cote):
    global size_maze
    size_square = size_maze / nbr_case_cote
    nbr_case = nbr_case_cote ** 2
    j = 0
    
    next()
        
    entrance(nbr_case_cote)

    next()

    '''make the grid:'''
    t.speed('fastest')
        
    '''grid colone'''
    for i in range(1, nbr_case_cote):
        t.up()
        t.goto(-size_maze / 2 + i * (size_square), size_maze / 2)
        t.down()
        t.goto(-size_maze / 2 + i * (size_square), -size_maze / 2)

    '''grid line'''
    for i in range(1, nbr_case_cote):
        t.up()
        t.goto(-size_maze / 2, size_maze / 2 - i * (size_square))
        t.down()
        t.goto(size_maze / 2, size_maze / 2 - i * (size_square))
    t.speed('slow')
    
    next()
    
    '''initialize the "maze" dict containing all the boxes'''

    '''for the line'''
    for k in range(nbr_case_cote):
        '''for the colone'''
        for i in range(nbr_case_cote):
            '''
            case n° = number of the case
            pos1x = x coordinate of top left corner
            pos1y = y coordinate of top left corner
            pos2x = x coordinate of bottom right corner
            pos2y = y coordinate of bottom right corner
            x = x coordinate of the center of the case
            y = y coordinate of the center of the case
            /after
            '''
            maze[j] = {'case n°': j + 1,
                       'pos1x': (-size_maze / 2 + i * size_square),
                       'pos1y': (size_maze / 2 - k * size_square),
                       'pos2x': (-size_maze / 2 + (i + 1) * size_square),
                       'pos2y': (size_maze / 2 - (k + 1) * size_square),
                       'x': ((-size_maze / 2 + i * size_square) + (-size_maze / 2 + (i + 1) * size_square)) / 2,
                       'y': ((size_maze / 2 - k * size_square) + (size_maze / 2 - (k + 1) * size_square)) / 2,
                       'nbr_pass': 0,
                       'nbr_way': 0,
                       'own_nbr': j,
                       }
            j += 1

    rdm_case = 0
    rdm_nbr = r.randint(1, 4)
    all = 0
    """while loop to make the maze's way by breaking down walls """

    while all < nbr_case:
        all = 0
        for i in range(nbr_case):
            if maze[0]['own_nbr'] == maze[i]['own_nbr']:
                all += 1
        rdm_own = maze[rdm_case]['own_nbr']
        count = 0

        if rdm_nbr == 1:
            if maze.get(rdm_case - 1) is not None and maze[rdm_case - 1]['own_nbr'] != maze[rdm_case]['own_nbr'] and maze[rdm_case]['pos1x'] == maze[rdm_case - 1]['pos2x']:
                erased(1, maze[rdm_case]['pos1x'], maze[rdm_case]['pos1y'], maze[rdm_case]['pos1x'],
                       maze[rdm_case]['pos2y'])
                maze[rdm_case - 1]['own_nbr'] = maze[rdm_case]['own_nbr']
                rdm_case = rdm_case - 1
                next()
            else:
                rdm_nbr = r.randint(1, 4)
                count += 1
            
        if rdm_nbr == 2:
            if maze.get(rdm_case - nbr_case_cote) is not None and maze[rdm_case - nbr_case_cote]['own_nbr'] != maze[rdm_case]['own_nbr']:
                erased(1, maze[rdm_case]['pos1x'], maze[rdm_case]['pos1y'], maze[rdm_case - nbr_case_cote]['pos2x'],
                       maze[rdm_case - nbr_case_cote]['pos2y'])
                maze[rdm_case - nbr_case_cote]['own_nbr'] = maze[rdm_case]['own_nbr']
                rdm_case = rdm_case - nbr_case_cote
                next()
            else:
                rdm_nbr = r.randint(1, 4)
                count += 1

        if rdm_nbr == 3:
            if maze.get(rdm_case + 1) is not None and maze[rdm_case + 1]['own_nbr'] != maze[rdm_case]['own_nbr'] and \
                    maze[rdm_case]['pos2x'] == maze[rdm_case + 1]['pos1x']:
                erased(1, maze[rdm_case + 1]['pos1x'], maze[rdm_case + 1]['pos1y'], maze[rdm_case]['pos2x'],
                       maze[rdm_case]['pos2y'])
                maze[rdm_case + 1]['own_nbr'] = maze[rdm_case]['own_nbr']
                rdm_case = rdm_case + 1
                next()
            else:
                rdm_nbr = r.randint(1, 4)
                count += 1

        if rdm_nbr == 4:
            if maze.get(rdm_case + nbr_case_cote) is not None and maze[rdm_case + nbr_case_cote]['own_nbr'] != maze[rdm_case]['own_nbr']:
                erased(1, maze[rdm_case + nbr_case_cote]['pos1x'], maze[rdm_case + nbr_case_cote]['pos1y'], maze[rdm_case]['pos2x'],
                       maze[rdm_case]['pos2y'])
                maze[rdm_case + nbr_case_cote]['own_nbr'] = maze[rdm_case]['own_nbr']
                rdm_case = rdm_case + nbr_case_cote
                next()
                
            elif count == 3:
                al = r.randint(0, len(maze) - 1)
                while maze[al]['own_nbr'] != maze[rdm_case]['own_nbr']:
                    al = r.randint(0, len(maze) - 1)
                rdm_case = al
                maze[rdm_case]['own_nbr'] = rdm_own
                
                
            else:
                rdm_nbr = r.randint(1, 3)
                continue
            
        rdm_nbr = r.randint(1, 4)

    entrance(nbr_case_cote)


    t.mainloop()

maze_function(10)
""""""